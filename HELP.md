# Discord Race Bot Help
## Commands
### Race Commands
| Command  | Description  | Usable In | Required Permissions |
| ------------ | ------------ | -------------- | ------------------ |
| !comment comment  | Adds a comment to the race  | Active race room | none |
| !startrace game \[mode\] \[\-\-async\] \[\-\-official\] \[\-\-event "event name"\] \[\-\-restrict "role name"\] | Creates a race with the specified criteria | Designated race channel | none |
| !done \[time\] | Finishes an active race | Active race room | none |
| !forfeit | Forfeits an active race | Active race room | none |
| !join race-name | Joins an open race | Designated race channel | none |
| !quit	| Quits a race you have joined | Active race room | none |
| !ready | Readies up for an open race | Active race room | none |
| !unready | Un-readies for an open race | Active race room | none |
| !entrants | Displays a list of entrants for the current race | Active race room | none |
| !close | Closes a race room, cancelling it if it was unfinished | Active race room | admin |
| !setteam team-name | Sets your team for the current race | Active race room | none |
| !randomteams numMembers \[\-\-dryrun\] \[\-\-everyone] | Places everyone in the race room into teams of numMembers size | Active race room | none |
| !showraces | Shows all active races, if used on a server limits the races to that server | Anywhere | none |
| !watch race-name | Allows you to view the race channel for a race | Designated race channel | none |
| !undone | Un-finishes a race and puts you back in the 'Running' state | Active race room | none |
| !multi | Displays a link of all active streams among race participants | Active race room | none |
| !clearteam | Sets your team value back to default | Active race room | none |
| !time | Displays a message showing how long the current race has been ongoing | Active race room | none |
| !setmode mode | Changes an active race's game mode | Active race room | none |
| !setmetadata key value | Adds a metadata value to the current race | Active race room | none |
| !startasync | Starts the current async race | Active async race room | Admin or Race Creator |
| !finishasync | Finishes the current async race | Active async race room | Admin or Race Creator |
| !forcevalue --entrant field value | Forces a value for a particular entrant, use with extreme caution. | Active race room | Admin |
| !wager amount | Wagers an amount of cookies on the race | Active race room | none |
| !kick username | Kicks a user out of the race | Active race room | Admin |
| !lock | Locks a room and prevents people from joining or the seed being updated | Active race room | Admin |
| !unlock | Undoes the !lock command | Active race room | Admin |
| !official | Sets the race as official | Active race room | Admin |
| !unofficial | Sets the race as unofficial | Active race room | Admin |
| !createvoicechannels | Creates voice channels for a 2v2 or team based race that only members of each team can see | Active race room | none |
| !donatecookies amount | Donates an amount of cookies to the active race. | Active race room | none |

###  Admin Commands
| Command  | Description  | Usable In | Required Permissions |
| ------------ | ------------ | -------------- | ------------------ |
| !setracecategory category | Sets the discord channel category where races will be placed | On a server | Admin |
| !setmaxvoice number | Sets the maximum number of voice channels that the bot will create for teams without an admin | On a server | Admin |
| !setmaxraces number | Sets the maximum number of active races that the bot will create without an admin | On a server | Admin |
| !setbotadmin role-name | Sets the role that is considered a bot admin | On a server | Admin |
| !setbotchannel channel-name | Sets the "Designated race channel" | On a server | Admin | 
| !setracealertchannel channel-name | Sets a channel to receive pings and alert messages when a race is created | On a server | Admin |
| !setracealertrole role-name | Sets a role to get pinged whenever a race is created | On a server | Admin |
| !setasyncracecategory category-name | Sets a separate race category for async races | On a server | Admin |
| !showconfig | Shows the current config settings for a server | On a server | Admin |
| !togglefeature feature-name | Toggles a feature on or off | On a server | Admin |
| !showfeatures | Shows all available features | On a server | Admin |
| !toggleracealertrole | Toggles the race alert role for the current user | On a server | none |
| !forcecheckfinish | Checks to see if a race is completed and marks it complete if necessary | Active race room | Admin |

### User Commands
| Command  | Description  | Usable In | Required Permissions |
| ------------ | ------------ | -------------- | ------------------ |
| !givecookie user \[amount\] | Gives the user 1 or an amount of your cookies | Anywhere | none |
| !userdetails user1 \[user2\] \[user3\] ... | Displays a table of user details for the given users | Anywhere | none |
| !setstream streamname | Sets a user's stream to twitch.tv/streamname | Anywhere | none |
| !cookiesheet \[number\] | Displays the top cookie holders | Anywhere | none |
| !resetpersonalbest | Resets your personal best that the bot understands | Anywhere | none |
| !eatcookie | Eats a cookie | Anywhere | none |

### Event Commands
| Command  | Description  | Usable In | Required Permissions |
| ------------ | ------------ | -------------- | ------------------ |
| !createevent "event name" | Creates as new event | Anywhere | none |
| !showevents | Shows all active events | Anywhere | none |
| !eventsettings "event name" \-\-options=options | Updates event settings | Anywhere | Event Creator or Lead |
| !finishevent "event name" | Concludes an event | Anywhere | Event Creator or Lead |
| !eventdetails "event name" | Displays event details | Anywhere | none |
| !addeventadmin "event name" \-\-user "username" \-\-role role | Adds a new admin to the event | Anywhere | Event Creator or Lead |
| !joinevent "event name" \[\-\-password password\] \[\-\-team teamname\ | Joins an event | Anywhere | none |
| !showeventadmins | Lists all of the admins for an event | Anywhere | none |

### Game Specific Commands
| Command  | Description  | Game | 
| ------------ | ------------ | -------------- |
| !ff1flags flags | Generates a Final Fantasy Randomizer seed with the given flags | ff1r |
| !smrpgflags flags | Generates a Super Mario RPG Randomizer seed with the given flags | smrpgr |
| !ff4flags flags | Generates a Final Fantasy 4 Free Enteprise seed with the given flags | ff4fe |
| !ff6wcflags flags \[\-\-description description\]| Generates a Final Fantasy 6 Worlds Collide seed with the given flags | ff6wc |
| !smb3rseed | Generates a new seed to use with the SMB3R Randomizer | smb3r |
| !weapon \[\-\-nospoiler\] | Chooses a weapon and aspect in Hades, optionally disabling hidden aspects | hades |
| !hadesheat \[\-\-minheat number\] \[\-\-maxheat number\] | Picks a random number for heat settings in hades. Defaults between 1-32 | hades |

### Bot Reward Commands
| Command  | Description  | Usable In | Required Permissions |
| ------------ | ------------ | -------------- | ------------------ |
| !showrewards | Shows a list of all available rewards | Anywhere | none |
| !buyreward reward | Buys an available reward for the listed amount of cookies | Anywhere | none |
| !featured | Shows all of the featured runners that bought the featured reward | Anywhere | none |
| !showbadges | Shows all available badges that can be earned | Anywhere | none |


