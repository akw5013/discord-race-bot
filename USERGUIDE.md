<h1>Discord Race Bot - User Setup Guide</h1>
<ul>
    <li>
        <h2>Discord Server Setup</h2>
        <ul>
            <li>Create a role on your server called <b>RaceBot Admin</b></li>
            <li>Create a channel on your server called <b>bot-controls</b></li>
            <li>Optionally:
                <ul>
                    <li>Create a category called <b>Races</b></li>
                    <li>Create a channel called <b>race-alerts</b></li>
                    <li>Create a role called <b>ping-to-race</b></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>Invite the bot to your server using the following link: https://discordapp.com/api/oauth2/authorize?client_id=507313123485548565&scope=bot&permissions=523365392 </li>
    <li>Ensure the bot's role name is <b>dr-race-bot</b></li>
    <li>Configure the bot using the following commands:
        <ul>
            <li><b>!setbotchannel your-desired-bot-channel</b> - It's important to use the text name of the channel and not the #Channel link</li>
            <li><b>!setbotrole your-desired-bot-role</b> - Again, use the text name of the role and not the @Role link. defaults to <b>dr-race-bot</b></li>
            <li><b>!setbotadmin your-desired-bot-admin-role</b> - Again, use the text name of the role and not the @Role link. defaults to <b>RaceBot Admin</b></li>
            <li><b>!setmaxraces number</b> - This is up to your discretion, how many races are allowed to be active at one time without an admin creating them. defaults to <b>4</b></li>
            <li><b>!setmaxvoice number</b> - Like above, how many voice channels can be created by the bot via non-admin users before an admin must step in. defaults to <b>3</b></li>
            <li><b>!setracecategory category</b> - The category that all bot created channels for live races will fall under, defaults to <b>Races</b></li>
            <li><b>!setasyncracecategory category</b> - The category that all bot created channels for async races will fall under, defaults to <b>Races</b></li>
            <li><b>!showfeatures</b> - Shows the toggleable features the bot supports.</li>
            <li><b>!togglefeature feature</b> - Toggles the selected feature on or off. Currently, the only feature supported is <b>asyncracing</b></li>
            <li><b>!setracealertchannel channel</b> - The channel that the bot will post race alerts to, pinging the race role if one is set.</li>
            <li><b>!setracealertrole role</b> - The role that will be pinged whenever a race is created.</li>
            <li><b>!showconfig</b> - Shows the current configuration settings for you server.</li>
        </ul>
    </li>
</ul>
<p>After following all of the above instructions, you can DM the bot or use <b>!help</b> in the bot channel or a race channel to get a list of available commands.</p>
<h2>Questions, Concerns, Feedback, or Need Help? - Join the RaceBot HQ Discord at https://discordapp.com/invite/nnryECe </h2>