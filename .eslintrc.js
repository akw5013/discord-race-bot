module.exports = {
    "extends": "airbnb-base",
    "env": {
        "es2020": true,
    },
    "parserOptions": {
        "ecmaVersion": 2020
    },
    "rules": {
        "no-extra-semi": ["error"],
        "semi": ["error", "never"],
        "max-len": ["warn", {
            "code": 180,
            "tabWidth": 4            
        }],
        "prefer-destructuring": ["warn", {
            "VariableDeclarator": {
                "array": true,
                "object": true
            },
            "AssignmentExpression": {
                "array": false,
                "object": false
            }
        }],
        "linebreak-style": ["off", "windows"],
        "eol-last": ["warn", "always"],
        "no-trailing-spaces": ["warn", {
            "skipBlankLines": false,
            "ignoreComments": true
        }],
        "indent": ["warn", 4]
    }
}