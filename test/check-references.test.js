const test = require('tape')
const path = require('path')
const fs = require('fs')

const getFilesFromDir = (dir, fileTypes) => {
    const filesToReturn = []
    const walkDir = (currentPath) => {
        const files = fs.readdirSync(currentPath)
        files.forEach((i) => {
            const curFile = path.join(currentPath, i)
            if (fs.statSync(curFile).isFile() && fileTypes.indexOf(path.extname(curFile)) !== -1) {
                filesToReturn.push(curFile.replace(dir, ''))
            } else if (fs.statSync(curFile).isDirectory()) {
                walkDir(curFile)
            }
        })
    }
    walkDir(dir)
    return filesToReturn
}

test('All the files get required correctly', (t) => {
    const allFiles = getFilesFromDir('src', '.js')
    allFiles.filter(f => f.endsWith('.js')).forEach((f) => {
        const reqPath = `../src${f.replace('\\', '/')}`
        const obj = require(reqPath) //eslint-disable-line
        t.notDeepEqual(obj, { }, `${reqPath} loaded properly`)
    })
    t.end()
})
