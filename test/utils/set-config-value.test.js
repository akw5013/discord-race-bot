const test = require('tape')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const loggerStub = { }
let configData = { }
const saveStub = sinon.fake()
const createStub = sinon.fake()
const configStub = async () => configData
const setConfigValue = proxyquire('../../src/utils/set-config-value', {
    './logger': loggerStub,
    '../db/get-server-config-by-id': configStub,
    '../db/save-server-config': saveStub,
    '../db/create-server-config': createStub,
})

test('It should return early if no guild is specified', async (t) => {
    t.is(await setConfigValue({ }), null, 'The function returns if a guild is not in the message.')
    t.same(await configStub(), { }, 'The config stub was unchanged.')
    t.end()
})
test('It should return early if no args are specified', async (t) => {
    t.is(await setConfigValue({ guild: true }, null), null, 'The function returns if no args are input.')
    t.is(await setConfigValue({ guild: true }, ['arg']), null, 'The function returns if less than 2 args are input.')
    t.same(await configStub(), { }, 'The config stub was unchanged.')
    t.end()
})
test('It should return if the user does not have permissions', async (t) => {
    t.is(await setConfigValue({ guild: true }, ['arg1', 'args2']), null, 'The function returns if no permissions are input.')
    t.is(await setConfigValue({ guild: true }, ['arg1', 'args2'], false), null, 'The function returns if no permissions are input.')
    t.same(await configStub(), { }, 'The config stub was unchanged.')
    t.end()
})
test('It should create a new value for a config that does not exist', async (t) => {
    loggerStub.logInfo = sinon.fake()
    configData = null
    const testMsg = { guild: { id: 'test' } }
    const testArgs = ['testkey', 'testvalue']
    const config = await setConfigValue(testMsg, testArgs, true)
    t.is(loggerStub.logInfo.callCount, 1, 'The log info method was called since a new config was created')
    t.is(createStub.callCount, 1, 'The create call was made')
    t.same(config, { id: 'test', testkey: 'testvalue' }, 'The config key was added.')
    t.end()
})
test('It should override an existing value for a config that exists', async (t) => {
    loggerStub.logInfo = sinon.fake()
    configData = { }
    const testMsg = { guild: { id: 'test' } }
    const testArgs = ['testkey', 'testvalue']
    configData.id = 'test'
    configData.testkey = 'testvalue2'
    const config = await setConfigValue(testMsg, testArgs, true)
    t.is(loggerStub.logInfo.callCount, 0, 'The log info method was not called since a new config was not created')
    t.same(config, { id: 'test', testkey: 'testvalue' }, 'The config key was added.')
    t.end()
})
