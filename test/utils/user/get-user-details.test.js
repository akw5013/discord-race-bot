const test = require('tape')
const proxyquire = require('proxyquire').noPreserveCache()
const sinon = require('sinon')
const userDataStub = require('../../data/user_details.test.json')

const getUserData = async id => userDataStub.find(u => u.id === id)
let saveStub = sinon.fake()
let createStub = sinon.fake()
let getUserDetails = proxyquire('../../../src/utils/user/get-user-details', {
    '../../db/user/get-user-by-id': getUserData,
    '../../db/user/save-user': saveStub,
})

const resetProxy = () => {
    saveStub = sinon.fake()
    createStub = sinon.fake()
    getUserDetails = proxyquire('../../../src/utils/user/get-user-details', {
        '../../db/user/get-user-by-id': getUserData,
        '../../db/user/save-user': saveStub,
        '../../db/user/create-user': createStub,
    })
}

test('It should return early if data is unavailable', async (t) => {
    t.is(await getUserDetails(null), null, 'It returns early if no message is specified.')
    t.is(await getUserDetails({ }), null, 'It returns early if no author is found.')
    t.end()
})

test('It should correctly retrieve user details for an existing user', async (t) => {
    const testMsg = {
        guild: { id: '411615349579186178' },
        author: { id: 'thisismytest1', username: 'Test' },
        member: { nickname: 'TestDisplay1' },
    }
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest1',
        name: 'Test',
        cookies: 6,
        guildDetails: { '411615349579186178': { nickname: 'TestDisplay1', display_name: 'Test Display' } },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is retrieved without updates.')
    t.is(saveStub.callCount, 0, 'The save stub was not called.')

    testMsg.member.nickname = 'TestDisplay2'
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest1',
        name: 'Test',        
        cookies: 6,
        guildDetails: { '411615349579186178': { nickname: 'TestDisplay1', display_name: 'Test Display' } },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is retrieved without an update to the nickname.')
    t.is(saveStub.callCount, 0, 'The save stub was not called to save the new details.')

    testMsg.member.nickname = 'Test Display'
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest1',
        name: 'Test',        
        cookies: 6,
        guildDetails: { '411615349579186178': { nickname: 'TestDisplay1', display_name: 'Test Display' } },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is retrieved without an update to the nickname.')
    t.is(saveStub.callCount, 0, 'The save stub was not called to save the new details.')

    testMsg.member.nickname = 'Test Display 2'
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest1',
        name: 'Test',
        cookies: 6,
        guildDetails: { '411615349579186178': { nickname: 'TestDisplay1', display_name: 'Test Display' } },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is retrieved without an update to the nickname and display name.')
    t.is(saveStub.callCount, 0, 'The save stub was called to save the new details.')

    t.end()
})

test('It should correctly create new details for a user that does not exist', async (t) => {
    resetProxy()
    const testMsg = {
        guild: { id: '411615349579186178' },
        author: { id: 'thisismytest9', username: 'Test 9' },
        member: { nickname: 'Test Guy', guild: { id: '411615349579186178' } },
    }
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest9',
        name: 'Test 9',
        cookies: 5,
        guildDetails: { },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is created and saved.')
    t.is(createStub.callCount, 1, 'The save stub was called.')

    t.end()
})

test('It should correctly create new details for a user that does not exist without guild details', async (t) => {
    resetProxy()
    const testMsg = {
        guild: { id: '411615349579186178' },
        author: { id: 'thisismytest8', username: 'Test 8' },
    }
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest8',
        name: 'Test 8',
        cookies: 5,
        guildDetails: { },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is created and saved.')
    t.is(createStub.callCount, 1, 'The save stub was called.')

    testMsg.member = { nickname: 'Test Guy', guild: { id: '411615349579186178' } }
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest8',
        name: 'Test 8',
        cookies: 5,
        guildDetails: { },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is not saved for a nickname update.')
    t.is(saveStub.callCount, 0, 'The save stub was not called.')

    t.end()
})

test('It should correctly update the username if it has been changed', async (t) => {
    resetProxy()
    const testMsg = {
        guild: { id: '411615349579186178' },
        author: { id: 'thisismytest1', username: 'Test2' },
        member: { nickname: 'TestDisplay1' },
    }
    t.same(await getUserDetails(testMsg), {
        id: 'thisismytest1',
        name: 'Test2',        
        cookies: 6,
        guildDetails: { '411615349579186178': { nickname: 'TestDisplay1', display_name: 'Test Display' } },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'The user is retrieved with updates to the username.')
    t.is(saveStub.callCount, 1, 'The save stub was called.')

    t.end()
})
