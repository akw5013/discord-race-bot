const test = require('tape')
const getNewUserDetails = require('../../../src/utils/user/get-new-user-details')

test('New user details are correctly returned', (t) => {
    const testUser = { username: 'Test', id: '123' }

    t.same(getNewUserDetails(testUser), {
        id: '123',
        name: 'Test',        
        cookies: 5,
        guildDetails: { },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    }, 'A user was created with the standard details without a guild member.')
    t.same(getNewUserDetails(testUser), {
        id: '123',
        name: 'Test',
        cookies: 5,
        guildDetails: { },
        race_details: {
            races_run: 0, races_first: 0, races_second: 0, races_third: 0, races_forfeit: 0, races_completed: [], personal_best: null, personal_best_race: null, seeds_rolled: 0,
        },
    })

    t.end()
})
