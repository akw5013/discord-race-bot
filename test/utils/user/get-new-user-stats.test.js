const test = require('tape')
const getNewUserStats = require('../../../src/utils/user/get-new-user-stats')

test('New user stats are returned with the default values', (t) => {
    t.same(getNewUserStats(), {
        race_details: {
            races_run: 0,
            races_first: 0,
            races_second: 0,
            races_third: 0,
            races_forfeit: 0,
            races_completed: [],
            personal_best: null,
            personal_best_race: null,
            seeds_rolled: 0,
        },
    }, 'New user details are created.')

    t.end()
})
