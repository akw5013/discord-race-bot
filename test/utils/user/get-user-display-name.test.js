const test = require('tape')
const getUserDisplayName = require('../../../src/utils/user/get-user-display-name')

test('It should return the user name if there is no display name', (t) => {
    t.is(getUserDisplayName({ name: 'test' }), 'test', 'The user name is returned if no guild is input.')
    t.is(getUserDisplayName({ name: 'test' }, { }), 'test', 'The user name is returned if there are no guild details.')
    t.is(getUserDisplayName({ name: 'test' }), 'test', 'The user name is returned if no matching guild details are found.')
    t.end()
})

test('It should return the user name if no display name is entered', (t) => {
    t.is(getUserDisplayName({ name: 'test' }), 'test', 'The user name is returned if no matching display name is found.')
    t.end()
})

test('It should return the display name when found', (t) => {
    t.is(getUserDisplayName({
        name: 'test',
        display_name: 'banana',
    }), 'banana', 'The user name is returned if no matching guild display name is found.')
    t.end()
})
