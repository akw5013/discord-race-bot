const test = require('tape')
const sinon = require('sinon')
const sendDirectMessage = require('../../src/utils/send-direct-message')

test('It should send a message to the users DM channel', async (t) => {
    const user = { dmChannel: { send: sinon.fake() } }
    await sendDirectMessage(user, 'This is a test message.')
    t.equals(1, user.dmChannel.send.callCount, 'The dm channel was called once')
    t.equals('This is a test message.', user.dmChannel.send.getCall(0).lastArg, 'The message passed in was sent to the channel')
    t.end()
})
test('It should not send an empty message', async (t) => {
    const user = { dmChannel: { send: sinon.fake() } }
    await sendDirectMessage(user, '')
    await sendDirectMessage(user, null)
    await sendDirectMessage(user, undefined)

    t.equals(0, user.dmChannel.send.callCount, 'Empty string, null, and undefined messages were not sent.')
    t.end()
})
test('It should create the DM channel if one does not already exist', async (t) => {
    const user = { createDM: sinon.fake() }
    await sendDirectMessage(user, 'Test message')
    t.equals(1, user.createDM.callCount, 'The createDM method was called')
    t.end()
})
