const test = require('tape')
const parseMessageArgs = require('../../src/utils/parse-message-args')

test('It should correctly parse out the arguments with quoted arguments', (t) => {
    const message = { }
    t.is(parseMessageArgs(message), null, 'Null is returned if the message has no content.')

    message.content = ''
    t.is(parseMessageArgs(message), null, 'Null is returned if the message content is empty.')

    message.content = '!help'
    t.same(parseMessageArgs(message), ['!help'], 'An array with "!help" is returned.')

    message.content = '!stats $entrant Supremacy'
    t.same(parseMessageArgs(message), ['!stats', '$entrant', 'Supremacy'], 'An array is returned with all elements parsed.')

    message.content = '!userdetails "Cecilia Lynn Adelhyde" $entrant Supremacy'
    t.same(parseMessageArgs(message), [
        '!userdetails',
        'Cecilia Lynn Adelhyde',
        '$entrant',
        'Supremacy',
    ], 'An array is returned with all elements correctly parsed with quotes surrounding multi-space args')

    message.content = '!join  ff4fe-abcde  $entrant  Supremacy'
    t.same(parseMessageArgs(message), ['!join', 'ff4fe-abcde', '$entrant', 'Supremacy'], 'An array is returned with empty arguments removed')

    t.end()
})

test('It should deal with smart quotes', (t) => {
    const message = { content: '“Fathers Against Rude Television”' }
    t.same(parseMessageArgs(message), [
        'Fathers Against Rude Television',
    ], 'The smart quotes were parsed correctly')
    t.end()
})
