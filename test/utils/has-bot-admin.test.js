const test = require('tape')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const getConfigValue = async () => 'RaceBot Admin'
const hasBotAdmin = proxyquire('../../src/utils/has-bot-admin', { './get-config-value': getConfigValue })

test('A call without a message should return false', async (t) => {
    t.equals(await hasBotAdmin(null), false, 'A null input returns false.')
    t.end()
})
test('A call without a guild should return false', async (t) => {
    t.equals(await hasBotAdmin({ test }), false, 'A message without a guild returns false.')
    t.end()
})
test('A call without a member returns false', async (t) => {
    t.equals(await hasBotAdmin({ guild: {} }), false, 'A message without a guild member returns false.')
    t.end()
})
test('A call with a user without the appropriate role returns false', async (t) => {
    const message = {
        guild: sinon.fake(),
        member: {
            roles: {
                cache: [
                    { name: 'Test' },
                ],
            },
        },
    }
    t.equals(await hasBotAdmin(message), false, 'The user does not have the appropriate role and should not have permissions')
    t.end()
})
test('A call with a user with the appropriate role returns true', async (t) => {
    const message = {
        guild: sinon.fake(),
        member: {
            roles: {
                cache: [
                    { name: 'RaceBot Admin' },
                ],
            },
        },
    }
    t.equals(await hasBotAdmin(message), true, 'The user has the appropriate role and has admin permissions')
    t.end()
})
test('The global bot admin should always be admin', async (t) => {
    process.env.GLOBAL_ADMIN_ID = '123456'
    const message = {
        author: {
            id: '123456'
        }
    }
    t.equals(await hasBotAdmin(message), true, 'The user has the appropriate role and has admin permissions')
    t.end()
})
