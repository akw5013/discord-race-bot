const test = require('tape')
const proxyquire = require('proxyquire')

const utilsStub = { }
const checkMessage = proxyquire('../../src/utils/check-message', { '../commands/race/race-utils': utilsStub })
test('False is returned if no message is provided', async (t) => {
    t.is(await checkMessage(null, null), false, 'Check message returns false when null is input.')
    t.is(await checkMessage(undefined, null), false, 'Check message returns false when undefined is input.')
    t.end()
})
test('False is returned if checkGuild option is specified and the message has no guild', async (t) => {
    t.is(await checkMessage({ }, { checkGuild: true }), false, 'Check message returns false when checkGuild is specified and there is no guild')
    t.end()
})
test('False is returned if channelType option is specified and the input type is not equal', async (t) => {
    const testMsg = { channel: { type: 'test' } }
    t.is(await checkMessage(testMsg, { channelType: 'dm' }), false, 'Check message returns false when the channelType does not match')
    t.end()
})
test('False is returned if channelName option is specified and the input channel is not equal', async (t) => {
    const testMsg = { channel: { type: 'test', name: 'test-channel' } }
    t.is(await checkMessage(testMsg, { channelName: 'not-test' }), false, 'Check message returns false when the channelType does not match')
    t.end()
})
test('False is returned if checkArgs is specified and no args are present in the message content', async (t) => {
    const testMsg = { content: '' }
    t.is(await checkMessage(testMsg, { checkArgs: true }), false, 'Check message returns false when there are no args present.')
    testMsg.content = 'This'
    t.is(await checkMessage(testMsg, { checkArgs: true }), false, 'Check message returns false when there is content but no args.')
    t.end()
})
test('False is returned if checkCommand is specified and the content does not start with ! or .', async (t) => {
    const testMsg = { content: 'test' }
    t.is(await checkMessage(testMsg, { checkCommand: true }), false, 'Check message returns false when the content does not start with . or !')
    t.end()
})
test('False is returned when checkRace is specified and no race is returned', async (t) => {
    utilsStub.getCurrentRace = async () => null
    const testMsg = { channel: { name: 'test' } }
    t.is(await checkMessage(testMsg, { checkRace: true }), false, 'Check message returns false when there is no race present.')
    t.end()
})
test('True is returned when there is a message and no options are specified', async (t) => {
    t.is(await checkMessage({ content: 'Test' }, null), true, 'Check message returns true if the message is defined and options are not.')
    t.end()
})
test('True is returned when no options are specified', async (t) => {
    utilsStub.getCurrentRace = async () => ({ })
    const testMsg = { guild: { }, channel: { type: 'test', name: 'test-name' }, content: '!test me' }
    const options = { }
    t.is(await checkMessage(testMsg, options), true, 'Check message returns true when no criteria is specified.')
    t.end()
})
test('True is returned when all options are specified, and all have passing criteria', async (t) => {
    utilsStub.getCurrentRace = async () => ({ })
    const testMsg = { guild: { }, channel: { type: 'test', name: 'test-name' }, content: '!test me' }
    const options = {
        checkGuild: true,
        channelType: 'test',
        channelName: 'test-name',
        checkArgs: true,
        checkCommand: true,
        checkRace: true,
    }
    t.is(await checkMessage(testMsg, options), true, 'Check message returns true when all criteria is specified and met')
    t.end()
})
