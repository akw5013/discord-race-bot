const test = require('tape')
const parseFilters = require('../../../src/utils/stats/parse-filters')

test('Empty arrays are correctly returned when invalid input is passed in', (t) => {
    t.same(parseFilters(), [], 'Empty array was returned if undefined was passed in.')
    t.same(parseFilters([]), [], 'Empty array was returned if no args were passed in.')
    t.same(parseFilters(['hello']), [], 'Empty array was returned if only a single arg was passed in.')
    t.same(parseFilters(['hello', 'is', 'it', 'me', "you're", 'looking', 'for']), [], 'Empty array was returned if only an odd number of args was passed in.')
    t.end()
})
test('Filters were correctly created when valid input is passed in', (t) => {
    t.same(parseFilters(['--entrant', 'hello']), [{ name: '--entrant', value: 'hello' }], 'A single filter was created successfully when passed in.')
    t.same(parseFilters(['--entrant', 'hello', '--time', 'test']), [
        { name: '--entrant', value: 'hello' },
        { name: '--time', value: 'test' },
    ], 'Multiple filters were created successfully when passed in.')
    t.end()
})
