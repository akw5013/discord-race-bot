const test = require('tape')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const loggerStub = {
    info: sinon.fake(),
    warn: sinon.fake(),
    error: sinon.fake(),
    debug: sinon.fake(),
}
const logger = proxyquire('../../src/utils/logger', { '../../config/winston': loggerStub })
test('The logger functions all work correctly', (t) => {
    logger.logInfo('Test Info')
    t.is(loggerStub.info.callCount, 1, 'The info method was called')
    t.is(loggerStub.info.getCall(0).lastArg, 'Test Info', 'The info method was called with the correct parameter')

    logger.logWarn('Test Warn')
    t.is(loggerStub.warn.callCount, 1, 'The warn method was called')
    t.is(loggerStub.warn.getCall(0).lastArg, 'Test Warn', 'The warn method was called with the correct parameter')

    logger.logError('Test Error')
    t.is(loggerStub.error.callCount, 1, 'The error method was called')
    t.is(loggerStub.error.getCall(0).lastArg, 'Test Error', 'The error method was called with the correct parameter')

    logger.logDebug('Test Debug')
    t.is(loggerStub.debug.callCount, 1, 'The debug method was called')
    t.is(loggerStub.debug.getCall(0).lastArg, 'Test Debug', 'The debug method was called with the correct parameter')

    t.end()
})
