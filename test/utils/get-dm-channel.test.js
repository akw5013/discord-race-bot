const test = require('tape')
const sinon = require('sinon')
const getDMChannel = require('../../src/utils/get-dm-channel')

test('It should create a DM channel for a user where one does not exist', async (t) => {
    const user = { createDM: sinon.fake() }
    await getDMChannel(user)
    t.equals(1, user.createDM.callCount, 'createDM should be called once')
    t.end()
})
test('It should return the DM channel for a user where it already exists', async (t) => {
    const user = { dmChannel: 'Test' }
    t.equals(user.dmChannel, await getDMChannel(user), 'The dmChannel should be pulled from the user')
    t.end()
})
