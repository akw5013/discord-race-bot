const test = require('tape')
const client = require('../../bot')

test('The bot is successfully able to connect without issues', async (t) => {
    await client.login(process.env.DISCORD_CLIENT_TOKEN)
    t.pass('The bot successfully connected without any errors.')
    await client.destroy()
    t.pass('The client was successfully destroyed.')
    t.end()
})
