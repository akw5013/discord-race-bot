const test = require('tape')
const path = require('path')
const fs = require('fs')

test('The Bot-Join.mp3 file exists', (t) => {
    const filePath = path.resolve('./src/sounds/Bot-Join.mp3')
    try {
        const res = fs.readFileSync(filePath)
        t.not(res, undefined, 'The file was defined')
        t.not(res, null, 'The file was not null')
        t.end()
    } catch (e) {
        t.end(e)
    }
})
