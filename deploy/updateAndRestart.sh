#!/bin/bash

set -e
echo "Changing to discord-race-bot"
cd ~/apps/discord-race-bot
echo "Pulling latest version"
git checkout .
git pull
echo "Running npm install"
source ~/.nvm/nvm.sh
npm ci --omit=dev
echo "Restarting application"
pm2 restart discord-race-bot
echo "Completed restarting"