require('dotenv').config()
const client = require('./bot')

// https://discordapp.com/api/oauth2/authorize?client_id=507313123485548565&scope=bot&permissions=523365392  - Prod link
// https://discordapp.com/api/oauth2/authorize?client_id=521763576982274048&scope=bot&permissions=523365392  - Test link
client.login(process.env.DISCORD_CLIENT_TOKEN)
