const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (eventId, race) => {
    if (!eventId) {
        return null
    }
    const uri = createApiConnection(`events/${eventId}/races/${race.id}/process`)
    const response = await makeCallAndGetResponse(uri, race, 'POST')
    return response
}
