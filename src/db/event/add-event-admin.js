const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (event, admin) => {
    const uri = createApiConnection('events', `${event.id}/admins`)
    const response = await makeCallAndGetResponse(uri, admin, 'POST')
    return response
}
