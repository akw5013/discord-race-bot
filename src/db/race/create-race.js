const saveRowWithId = require('../util/save-row-with-id')

module.exports = async (race) => saveRowWithId('races', race)
