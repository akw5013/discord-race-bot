const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')
const raceCache = require('../../cache/race-cache')

module.exports = async (race, finisher) => {
    raceCache.clearCacheValue(race.id)
    raceCache.clearCacheValue(race.key)
    race.finishers.push(finisher)
    const uri = createApiConnection('races', `${race.id}/finishers`)
    const response = await makeCallAndGetResponse(uri, finisher, 'POST')
    return response
}
