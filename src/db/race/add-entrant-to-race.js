const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')
const raceCache = require('../../cache/race-cache')

module.exports = async (race, entrant) => {
    raceCache.clearCacheValue(race.id)
    raceCache.clearCacheValue(race.key)
    race.entrants.push(entrant)
    const uri = createApiConnection('races', `${race.id}/entrants`)
    const response = await makeCallAndGetResponse(uri, entrant, 'POST')
    return response
}
