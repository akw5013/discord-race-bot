const saveRowWithId = require('./util/save-row-with-id')
const guildCache = require('../cache/guild-cache')

module.exports = async (config) => {
    await guildCache.clearCacheValue(config.id)
    await saveRowWithId('guilds', config, config.id)
}
