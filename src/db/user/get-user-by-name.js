const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')
const userCache = require('../../cache/user-cache')

module.exports = async (name, guildId) => {
    if (!name) {
        return null
    }
    const lowerCaseName = name.toLowerCase()
    const cachedValue = await userCache.getValue(lowerCaseName)
    if (!cachedValue) {
        let uri = createApiConnection(`users?name=${encodeURIComponent(lowerCaseName)}`)
        if (guildId) {
            uri = uri.concat(`&guild=${encodeURIComponent(guildId)}`)
        }
        const response = await makeCallAndGetResponse(uri, null, 'GET')
        await userCache.updateCacheValue(lowerCaseName, response)
        return response
    }
    return cachedValue
}
