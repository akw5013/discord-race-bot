const saveRowWithId = require('../util/save-row-with-id')

module.exports = async (user) => saveRowWithId('users', user)
