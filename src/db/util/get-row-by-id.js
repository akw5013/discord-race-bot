const createApiConnection = require('./create-api-connection')
const makeCallAndGetResponse = require('./make-call-and-get-response')

module.exports = async (tableName, id) => {
    if (!id) {
        return null
    }
    const uri = createApiConnection(tableName, id)
    const response = await makeCallAndGetResponse(uri, null, 'GET')
    return response
}
