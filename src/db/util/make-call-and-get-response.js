require('dotenv').config()
const fetch = require('node-fetch')
const logger = require('../../utils/logger')

module.exports = async (uri, object, method) => {
    const bodyObject = object
    logger.logInfo([method, uri])
    const basicHeaders = {
        'Content-Type': 'application/json',
        apikey: process.env.DR_BOT_API_KEY,
        'X-REQUEST-SOURCE': `Discord-Race-Bot-${process.env.ENV_NAME}`,
    }
    const requestOptions = {
        method,
        body: method !== 'GET' ? JSON.stringify(bodyObject) : null,
        headers: basicHeaders,
    }
    const response = await fetch(uri, requestOptions)
    try {
        logger.logInfo(`Response Status: ${response.status}`)
        if (!String(response.status).startsWith('2')) {
            logger.logError(`Error returned from service: ${response.status}: ${await response.text()}`)
            return null
        }
        if (response.status === 204) {
            return null
        }
        const responseJson = await response.json()
        if (!responseJson) {
            return null
        }
        if (responseJson.err) {
            logger.logError(`Error returned from service: ${response.status}: ${JSON.stringify(responseJson.err)}`)
            return null
        }
        if (responseJson.items) {
            if (responseJson.currentPage !== responseJson.numPages) {
                const additionalCalls = []
                for (let nextPage = responseJson.currentPage + 1; nextPage <= responseJson.numPages; nextPage += 1) {
                    const newUri = uri.includes('?') ? uri.concat(`&page=${nextPage}`) : uri.concat(`?page=${nextPage}`)
                    additionalCalls.push(fetch(`${newUri}`, requestOptions))
                }
                const additionalData = await Promise.all(additionalCalls)
                const additionalItems = await Promise.all(additionalData.map((d) => d.json()))
                const allOtherItems = additionalItems.map((d) => d.items)
                allOtherItems.forEach((page) => responseJson.items.push(...page))
            }
            return responseJson.items
        }
        return responseJson
    } catch (err) {
        logger.logError(err)
        return null
    }
}
