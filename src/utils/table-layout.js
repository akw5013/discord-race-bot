module.exports = (headers, data) => {
    if (!headers || headers.length === 0) {
        return null
    }
    if (!data || data.length === 0) {
        return headers.join(' ')
    }
    const [rowSample] = data
    const dataPoints = Object.keys(rowSample)
    let maxIndex = headers.length
    if (headers.length !== dataPoints.length) {
    // Mismatch in headers and data length, only use the lesser number
        if (dataPoints.length < headers.length) {
            maxIndex = dataPoints.length
        }
    }
    const maxLengths = []
    // Figure out the longest length for each column, including the header name
    for (let i = 0; i < maxIndex; i += 1) {
        const headerLength = headers[i].length
        const dataLength = Math.max(...data.map((row) => String(row[dataPoints[i]]).length))
        const maxLength = headerLength > dataLength ? headerLength : dataLength
        maxLengths.push(maxLength)
    }
    // Put everything together
    let tableText = ''
    for (let i = 0; i < maxIndex; i += 1) {
        const header = headers[i]
        tableText += `${header.padEnd(maxLengths[i], ' ')} `
    }
    tableText += '\n'
    tableText += '-'.padEnd(maxLengths.reduce((total, num) => total + num) + maxIndex, '-')
    tableText += '\n'
    data.forEach((dataRow) => {
        for (let i = 0; i < maxIndex; i += 1) {
            const column = String(dataRow[dataPoints[i]])
            tableText += `${column.padEnd(maxLengths[i], ' ')} `
        }
        tableText += '\n'
    })
    return tableText
}
