module.exports = (args) => {
    const filters = []
    if (args && args.length > 0) {
        if (args.length % 2 !== 0) {
            return filters
        }
        for (let i = 0; i < args.length; i += 2) {
            filters.push({ name: args[i], value: args[i + 1] })
        }
    }
    return filters
}
