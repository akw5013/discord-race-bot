// Entity can be a role or a user
module.exports = async (channel, entity, permissionSet) => {
    if (!permissionSet || !channel || !entity) {
        return null
    }
    if (channel.permissionOverwrites.cache.find((po) => po.id === entity)) {
        return channel.permissionOverwrites.edit(entity, permissionSet)
    }
    return channel.permissionOverwrites.create(entity, permissionSet)
}
