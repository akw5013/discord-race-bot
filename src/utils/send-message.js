const MAX_DM_LENGTH = 2000
const MAX_GUILD_CHANNEL_LENGTH = 2000
module.exports = async (channel, message) => {
    if (!channel) { return null }
    if (!message) { return null }
    // Sanity check in case we forget an await and pass a promise in here
    const msgText = await Promise.resolve(message)
    if (!msgText) { return null }
    const maxLength = channel.type === 'DM' ? MAX_DM_LENGTH : MAX_GUILD_CHANNEL_LENGTH
    if (msgText.length > maxLength) {
        const messageParts = Math.ceil(msgText.length / maxLength)
        const beforeMessage = '```\n'
        const afterMessage = '\n```'
        const messages = []
        const fullMessage = msgText.replace('```', '') // Strip out the formatting if it was there before
        let nextStartIndex = 0
        let nextLength = (maxLength - beforeMessage.length - afterMessage.length)
        for (let i = 0; i <= messageParts; i += 1) {
            // Since this will mostly be used for splitting tables, try to make sure we split at the row
            let messagePart = `${fullMessage.slice(nextStartIndex, nextLength)}`
            const indexOfLastLine = messagePart.lastIndexOf('\n')
            if (indexOfLastLine > 0) {
                messagePart = messagePart.substring(0, indexOfLastLine)
            }
            nextStartIndex += messagePart.length
            nextLength = nextStartIndex + (maxLength - beforeMessage.length - afterMessage.length)
            if (messagePart.length > 0 && messagePart.trim() !== '```') {
                messagePart = `${beforeMessage}${messagePart}${afterMessage}`
                messages.push(channel.send(messagePart))
            }
        }
        return Promise.all(messages)
    }
    return channel.send(msgText)
}
