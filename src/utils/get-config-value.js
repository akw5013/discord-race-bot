const logger = require('./logger')
const getServerConfig = require('../db/get-server-config-by-id')
const setConfigValue = require('./set-config-value')

module.exports = async (message, key, defaultValue) => {
    if (!message.guild) { return defaultValue }
    const serverConfig = await getServerConfig(message.guild.id)
    if (serverConfig && serverConfig[key]) {
        return serverConfig[key]
    }
    logger.logInfo(`${key} not found, returning ${defaultValue}`)
    if (defaultValue) {
        logger.logInfo(`Setting ${key} to default of ${defaultValue}`)
        setConfigValue(message, [key, defaultValue], true)
    }
    return defaultValue
}
