const CONSTANTS = require('../constants')
const getConfigValue = require('./get-config-value')
const { isGlobalAdmin } = require('./security/security-utils')

module.exports = async (message) => {
    if (!message) { return false }
    if (isGlobalAdmin(message?.author)) { return true }
    if (!message.guild) { return false }
    const { member } = message
    if (!member) { return false }
    const adminRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.BOT_ADMIN_ROLE, CONSTANTS.CONFIG.DEFAULT_ADMIN_ROLE)
    const { roles } = member
    const hasBotAdminRole = (!!roles.cache.find((item) => item.name === adminRole))
    return hasBotAdminRole
}
