module.exports = (user, badge) => user && user.badges && user.badges.some((b) => b.name === badge.name)
