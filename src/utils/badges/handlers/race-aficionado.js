const THRESHOLD = 1000
module.exports = (message, race, user, badge) => {
    if (!user) { return null }
    if (user.race_details.races_run >= THRESHOLD) {
        const award = { earnedFrom: race.key, ...badge }
        return award
    }
    return null
}
