const getRandomNumber = require('./get-random-number')

module.exports = (options, criteria = () => true) => {
    const toChooseFrom = options.filter(criteria)
    return toChooseFrom[getRandomNumber(0, toChooseFrom.length)]
}
