const getUserByName = require('../../db/user/get-user-by-name')

module.exports = async (name, guild) => {
    if (!name) { return null }
    if (guild && typeof guild === 'object' && guild.id) {
        return getUserByName(name, guild.id)
    }
    return getUserByName(name, guild)
}
