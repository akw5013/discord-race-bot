const getNewUserDetails = require('./get-new-user-details')
const saveUserData = require('../../db/user/save-user')
const createUserData = require('../../db/user/create-user')
const getUserById = require('../../db/user/get-user-by-id')

module.exports = async (message) => {
    if (!message || !message.author) { return null }
    const userId = message.author.id
    let userDetails = await getUserById(userId)
    if (!userDetails) {
        userDetails = getNewUserDetails(message.author)
        await createUserData(userDetails)
    }
    const updatedDetails = {}
    if (userDetails.name !== message.author.username) {
        userDetails.name = message.author.username
        updatedDetails.name = message.author.username
    }
    if (Object.keys(updatedDetails).length > 0) {
        await saveUserData(userDetails, updatedDetails)
    }
    return userDetails
}
