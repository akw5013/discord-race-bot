const logger = require('../logger')

module.exports = async (username, client) => {
    // First check for a match in the cached users, and if no matches then look in each guild
    let foundUser = client.users.cache.find((user) => user.username.toLowerCase() === username.toLowerCase())
    if (foundUser) { return foundUser }
    const allUsers = client.guilds.cache.map((guild) => guild.users.cache).reduce((arr, item) => arr.concat(item), []).filter((i) => i)
    foundUser = allUsers.find((user) => user.username.toLowerCase() === username.toLowerCase())
    if (!foundUser) {
        logger.warn(`No users found for ${username}`)
    }
    return foundUser
}
