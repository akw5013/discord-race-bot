const moment = require('moment')
const CONSTANTS = require('../../constants')
const saveUser = require('../../db/user/save-user')
const updateCookies = require('../../db/user/update-cookies')

module.exports = async (user, raceDetails) => {
    const updatedUser = user
    if (!user || !user.race_details) {
        return `User has no race profile.  This is an error, please inform DarkPaladin about the user in the ${raceDetails.placement} placement for ${raceDetails.key}`
    }
    const {
        placement,
        key,
        time,
        amountWon,
    } = raceDetails
    switch (placement) {
    case 1:
        updatedUser.race_details.races_first += 1
        break
    case 2:
        updatedUser.race_details.races_second += 1
        break
    case 3:
        updatedUser.race_details.races_third += 1
        break
    default:
        break
    }
    updatedUser.race_details.races_run += 1
    if (time !== CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        updatedUser.race_details.races_completed.push(key)
        if (updatedUser.race_details.personal_best) {
            const pb = moment.duration(updatedUser.race_details.personal_best, 'HH:mm:ss.SSS').asMilliseconds()
            const thisTime = moment.duration(time, 'HH:mm:ss.SSS').asMilliseconds()
            if (thisTime < pb) {
                updatedUser.race_details.personal_best = time
                updatedUser.race_details.personal_best_race = key
            }
        } else {
            updatedUser.race_details.personal_best = time
            updatedUser.race_details.personal_best_race = key
        }
        await updateCookies(updatedUser, amountWon, 'Race Completion', `Placed ${placement} in race ${key}`)
    } else {
        updatedUser.race_details.races_forfeit += 1
    }
    await saveUser(updatedUser, { race_details: updatedUser.race_details })
    return null
}
