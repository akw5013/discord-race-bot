const getNewUserStats = require('./get-new-user-stats')

module.exports = (user) => {
    let userDetails = {
        name: user.username,
        cookies: 5,
        id: user.id,
        guildDetails: { },
    }
    userDetails = Object.assign(userDetails, getNewUserStats())
    return userDetails
}
