const logger = require('./logger')

module.exports = async (guild, channelName, channelType, channelCategory, adminRole, botRole) => {
    if (!guild) { return null }
    let parent = null
    if (channelCategory) {
        const cat = guild.channels.cache.find((ch) => ch.name === channelCategory)
        if (cat) {
            parent = cat
        }
    }
    const isAsync = channelName.includes('async') && !channelName.includes('spoilers')
    const atEveryone = guild.roles.cache.find((role) => role.name === '@everyone')
    const guildAdminRole = guild.roles.cache.find((role) => role.name === adminRole)
    let guildBotRole = guild.roles.cache.find((role) => role.name === botRole)
    // Backwards compatibility jank
    if (!guildBotRole) {
        guildBotRole = guild.roles.cache.find((role) => role.name === botRole.replace('discord', 'dr'))
        if (!guildBotRole) {
            guildBotRole = guild.roles.cache.find((role) => role.name === botRole.replace('dr', 'discord'))
        }
    }
    const options = {
        type: channelType,
        name: channelName,
        parent,
        permissionOverwrites: [
            {
                id: atEveryone,
                deny: ['SEND_MESSAGES', 'VIEW_CHANNEL'],
            },
            {
                id: guildBotRole,
                allow: ['VIEW_CHANNEL', 'MANAGE_CHANNELS', 'MOVE_MEMBERS', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
            },
            {
                id: guildAdminRole,
                allow: ['VIEW_CHANNEL', 'MANAGE_CHANNELS', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
            },
        ],
    }
    if (isAsync) {
        options.permissionOverwrites[0].deny.push('USE_EXTERNAL_EMOJIS')
    }
    const channel = await guild.channels.create(channelName, options)
    logger.logDebug(`Channel ${channel.name} created successfully`)
    return channel
}
