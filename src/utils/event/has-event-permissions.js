const PERM_LEVELS = require('../../constants').EVENT_PERMISSIONS
const { isGlobalAdmin } = require('../security/security-utils')

module.exports = (event, user, adminLevel) => {
    if (isGlobalAdmin(user)) {
        return true
    }
    const eventAdmin = event.admins.find((ad) => ad.id === user.id)
    if (!eventAdmin) {
        return false
    }
    const { role } = eventAdmin
    if (adminLevel === role) {
        return true
    }
    const requiredLevel = PERM_LEVELS.find((level) => level.name === adminLevel).level
    const currentLevel = PERM_LEVELS.find((level) => level.name === role).level
    return requiredLevel <= currentLevel
}
