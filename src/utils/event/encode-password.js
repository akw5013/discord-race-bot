const SECRET = process.env.EVENT_SECRET

module.exports = (password) => Buffer.from(password + SECRET).toString('base64')
