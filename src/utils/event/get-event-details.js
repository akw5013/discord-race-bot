const buildTable = require('../table-layout')

const buildEventSettings = (event) => {
    const settings = [
        'pointsystem',
        'visibility',
        'pointmax',
        'pointmin',
        'pointstep',
        'pointrounding',
        'pointforfeit',
        'rating',
        'tau',
        'rd',
        'vol',
    ]
    const messages = []
    settings.forEach((setting) => {
        if (event[setting]) {
            messages.push(`${setting}=${event[setting]}`)
        }
    })
    return messages.join(' ')
}

module.exports = (event) => {
    let eventResults = '```\n'
    const entrantHeaders = ['Name', 'Standing', 'Points', 'Races']
    eventResults += `Event Name: ${event.name}\n`
    eventResults += `Event Type: ${event.type}\n`
    eventResults += `Event Settings: ${buildEventSettings(event)}\n`
    if (event.finishTime) { eventResults += `End Time: ${event.finishTime}\n` }
    if (event.entrantType === 'TEAM') {
        const teamDetails = event.teams.sort((a, b) => b.points - a.points).map((team) => ({
            name: team.name,
            standing: team.placement,
            points: team.points,
            races: team.races.length,
        }))
        eventResults += buildTable(entrantHeaders, teamDetails)
    } else {
        const entrantDetails = event.entrants.sort((a, b) => b.points - a.points).map((entrant) => ({
            name: entrant.name,
            standing: entrant.placement,
            points: entrant.points,
            races: entrant.races.length,
        }))
        eventResults += buildTable(entrantHeaders, entrantDetails)
    }
    eventResults += '\n```'
    return eventResults
}
