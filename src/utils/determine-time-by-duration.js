const moment = require('moment')

module.exports = (duration) => moment.utc(duration.asMilliseconds()).format('HH:mm:ss.SSS')
