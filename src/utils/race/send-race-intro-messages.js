module.exports = async (race, channel) => {
    let message = `
\`\`\`
Basic Commands
--------------
!ready - (Indicate you are ready)
!unready - (Indicate you are no longer ready)
!quit - (Leave the race)
!forfeit - (Forfeit the race)
!done - (Finish the race)
!wager amount - (Wagers cookies on the race!)
!undone - (Indicate the !done was a mistake and go back to 'Running')
!setmetadata key value - (Sets the metadata for a race - can only be done via race creator or admin)
`
    // TODO: Refactor into not terrible
    if (race.key.startsWith('ff1r')) {
        message += `
Additional Commands
-------------------
!ff1flags flagstring - (Generates a FF1R seed given a flagstring)
`
    } else if (race.key.startsWith('ff4fe')) {
        message += `
Additional Commands
-------------------
!ff4flags [--hidden] flagstring - (Generates a FF4FE seed given a flagstring)
!ff4beta [--hidden] flagstring - (Generates a FF4FE seed given a flagstring - using the beta site)
!omg - (Generates a FF4FE seed with the Omnidexterous Memers Guild flagset)
!zz6 - (Generate a FF4FE seed with the Zemus Zone 6 flagset)
`
    } else if (race.key.startsWith('smrpgr')) {
        message += `
Additional Commands
-------------------
!smrpgflags flagstring - (Generates a SMRPGR seed given a flagstring)
`
    } else if (race.key.startsWith('hades')) {
        message += `
Additional Commands
-------------------
!weapon [--nospoiler] - Choose a random weapon. If --nospoiler is included, hidden aspects will be excluded.
!hadesheat [--minheat x] [--maxheat y] - Chooses a heat between minHeat and maxHeat (default Min=0, Max=32)
!rollhades [--nospoiler] [--minheat x] [--maxheat y] - Combines !weapon and !hadesheat
`
    } else if (race.key.startsWith('ff6wc')) {
        message += `
Additional Commands
-------------------
!ff6wcflags flags [--description description] - Generates a FF6WC seed with the given flagstring and an optional description using ff6worldscollide.com
!ff6wclegacy flags [--description description] - Generates a FF6WC seed with the given flagstring and an optional description using ff6wc.com
`
    } else if (race.key.startsWith('ctjot')) {
        message += `
Additional Commands
-------------------
!ctjotpreset preset [--seed seed] - Updates race metadata given the current presets for the CTJoT randomizer
`
    } else if (race.key.startsWith('ff4fd')) {
        message += `        
Additional Commands
-------------------
!ff4fdseed - Gives a number between 1 and 18,446,744,073,709,551,615
`
    }
    message += '\n```'
    channel.send(message)
    if (race.async) {
        channel.send('Don\'t forget to `!startasync` to finalize the race details!\nDon\'t forget to `!finishasync` when the race is over!')
    }
    if (race.official) {
        channel.send('This is an **official** race room.  Only admins may modify the race details, such as seed.')
    }
}
