module.exports = async (client, race) => {
    if (!client) { return }
    if (!race) { return }
    if (!race.messages || race.messages.length < 1) { return }
    race.messages.forEach((alertDetails) => {
        const guild = client.guilds.cache.find((gld) => gld.id === alertDetails.guildId)
        const channel = guild.channels.cache.find((chn) => chn.id === alertDetails.channelId)
        channel.messages.fetch(alertDetails.messageId)
            .then((msg) => {
                if (!msg) { return }
                msg.delete()
            })
    })
}
