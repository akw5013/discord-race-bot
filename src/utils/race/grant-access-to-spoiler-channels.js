const updateChannelPermissions = require('../update-channel-permissions')
const CONSTANTS = require('../../constants')

module.exports = (message, race) => {
    if (race.spoilerChannels.length > 0) {
        race.spoilerChannels.forEach((ch) => {
            const spoilerChannel = message.guild.channels.cache.find((chan) => chan.id === ch)
            if (spoilerChannel) {
                updateChannelPermissions(spoilerChannel, message.author, CONSTANTS.PERMISSION_SETS.READ_WRITE)
            }
        })
    }
}
