const CONSTANTS = require('../../constants')

module.exports = (race) => {
    if (!race) { return false }
    if (!race) { return false }
    if (!race.status) { return false }
    return race.status === CONSTANTS.RACE_STATUS.OPEN
}
