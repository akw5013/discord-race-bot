const isRaceOpen = require('./is-race-open')
const isRaceLocked = require('./is-race-locked')

module.exports = (race) => isRaceOpen(race) || isRaceLocked(race)
