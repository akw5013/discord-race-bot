const sendDirectMessage = require('../send-direct-message')

const SECONDS_BETWEEN_MESSAGES = 1
const MILLIS_BETWEEN_MESSAGES = SECONDS_BETWEEN_MESSAGES * 1000

module.exports = async (client, race, message) => {
    race.entrants.forEach(async (entrant, index) => {
        const raceGuild = client.guilds.cache.find((guild) => guild.id === race.guild.id)
        const user = await raceGuild.members.fetch(entrant.id)
        setTimeout(() => {
            sendDirectMessage(user, message)
        }, MILLIS_BETWEEN_MESSAGES * index)
    })
}
