const getConfigValue = require('../get-config-value')
const logger = require('../logger')
const sendMessage = require('../send-message')
const CONSTANTS = require('../../constants')

module.exports = async (message, alert, sendPing = false) => {
    if (!message.guild) { return null }
    const raceAlertChannelName = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.RACE_ALERT_CHANNEL, null)
    const raceAlertRoleName = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.RACE_ALERT_ROLE, null)
    if (!raceAlertChannelName) {
        return null
    }
    const raceAlertChannel = message.guild.channels.cache.find((channel) => channel.name === raceAlertChannelName)
    if (!raceAlertChannel) {
        logger.logWarn(`No channel found with name ${raceAlertChannelName}`)
        return null
    }
    const raceAlertRole = message.guild.roles.cache.find((role) => role.name === raceAlertRoleName)
    const newMessage = await sendMessage(raceAlertChannel, `${(sendPing && raceAlertRole) || 'Interested Race Parties'} - ${alert}`)
    return newMessage
}
