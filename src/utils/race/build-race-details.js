const moment = require('moment')
const displayTable = require('../table-layout')
const determineTimeByDuration = require('../determine-time-by-duration')
const calculateZScore = require('../stats/calculate-z-score')
const CONSTANTS = require('../../constants')

const getZScoreForEntrant = (scores, entrant) => {
    if (!scores || scores.length === 0) {
        return '-'
    }
    const entrantScore = scores.find((score) => score.id === entrant.id)
    if (!entrantScore) {
        return '-'
    }
    return entrantScore.score || '-'
}
const getAmountWon = (finishers, entrant) => {
    const finisher = finishers.find((f) => f.id === entrant.id)
    return (finisher && finisher.amountWon) || 'Unavailable'
}
module.exports = (race) => {
    let text = '```\n'
    text += `Race Key   : ${race.key}\n`
    text += `Server     : ${race.guild.name || 'Unknown'}\n`
    text += `Race Status: ${race.status}\n`
    text += `Official   : ${race.official}\n`
    text += `Created    : ${race.created}\n`
    if (race.async) {
        text += `Creator    : ${race.creator.name}\n`
    }
    if (race.startTime) {
        text += `Started    : ${race.startTime}\n`
    }
    if (race.finishTime) {
        text += `Completed  : ${race.finishTime}\n`
    }
    if (race.event) {
        text += `Event      : ${race.event.name}\n`
    }
    text += `Race Type  : ${race.game} (${race.mode})\n`
    if (race.metadata) {
        Object.keys(race.metadata).forEach((metakey) => {
            text += `${metakey.padEnd(11, ' ')}: ${race.metadata[metakey]}\n`
        })
    }
    const totalWager = race.entrants.reduce((total, en) => total + (en.wager || 0), 0)
    if (totalWager > 0) {
        text += `Total Wager: ${totalWager}\n`
    }
    const { donations } = race
    if (donations) {
        text += `Donations  : ${donations.map((donation) => `${donation.name}-${donation.amount}`).join('\n             ')}\n`
    }
    text += '\n'
    const entrants = race.entrants.filter((en) => en).sort((a, b) => {
    // Sort by placement if there is one, then sort by name if there's not
        if (a.placement && b.placement) {
            return a.placement - b.placement
        } if (a.placement) {
            return -a.placement
        } if (b.placement) {
            return b.placement
        }
        return (`${a.name}`).localeCompare(b.name)
    })
    const headers = ['Name', 'Wager', 'Team', 'Status', 'Placement', 'Finish Time', 'Z-Score', 'Comments', 'Amount Won']
    const isRaceFinished = race.status === CONSTANTS.RACE_STATUS.FINISHED
    const isRaceAsync = !!race.async
    const shouldHidePlacements = isRaceAsync && !isRaceFinished
    const zScores = isRaceFinished ? calculateZScore(entrants) : []

    const rows = entrants.map((entrant) => ({
        name: entrant.name || ' ',
        wager: entrant.wager || 0,
        team: entrant.team || ' ',
        status: entrant.status || ' ',
        placement: !shouldHidePlacements ? entrant.placement || '-' : '',
        finish: !shouldHidePlacements ? entrant.finish || '-' : '',
        zScore: !shouldHidePlacements ? getZScoreForEntrant(zScores, entrant) : '',
        comment: isRaceFinished ? entrant.comment || ' ' : '',
        amountWon: isRaceFinished ? getAmountWon(race.finishers, entrant) : '',
    }))
    text += displayTable(headers, rows)
    text += '\n'
    const teams = new Set(entrants.map((entrant) => entrant.team))
    const teamTimes = []
    teams.forEach((team) => {
        const teamFinishTimes = entrants.filter((entrant) => entrant.team === team).map((entrant) => entrant.finish)
        if (teamFinishTimes.length > 1) {
            if (teamFinishTimes.indexOf(CONSTANTS.ENTRANT_STATUS.FORFEIT) >= 0) {
                teamTimes.push({
                    id: team, finish: CONSTANTS.ENTRANT_STATUS.FORFEIT, avg: CONSTANTS.ENTRANT_STATUS.FORFEIT, isFinished: true, time: Infinity,
                })
            } else {
                const sumTimes = teamFinishTimes.reduce((total, time) => total + moment.duration(time, 'HH:mm:ss.SSS').asMilliseconds(), 0)
                const avgTime = sumTimes / teamFinishTimes.length
                const teamTime = determineTimeByDuration(moment.duration(sumTimes))
                const teamAvgTime = determineTimeByDuration(moment.duration(avgTime))
                teamTimes.push({
                    id: team,
                    finish: !shouldHidePlacements ? teamTime || '-' : '',
                    avg: !shouldHidePlacements ? teamAvgTime || '-' : '',
                    isFinished: !shouldHidePlacements && teamFinishTimes.every((f) => f),
                    time: !shouldHidePlacements ? moment.duration(sumTimes).asMilliseconds() || '-' : '',
                })
            }
        }
    })
    if (teamTimes.length > 0) {
        const teamZScores = isRaceFinished ? calculateZScore(teamTimes, 20) : []

        teamTimes.sort((a, b) => a.time - b.time)
        let finishedTeams = 1
        teamTimes.forEach((tt) => {
            const updated = tt
            if (tt.isFinished && !shouldHidePlacements) {
                updated.placement = !shouldHidePlacements ? finishedTeams || '-' : ''
                finishedTeams += 1
            } else {
                updated.placement = '-'
            }
            updated.score = getZScoreForEntrant(teamZScores, tt)
            // Clean up properties not used in the display table
            delete updated.isFinished
            delete updated.time
        })
        const teamHeaders = ['Team Name', 'Total Time', 'Average Time', 'Placement', 'Z-Score']
        text += displayTable(teamHeaders, teamTimes)
    }
    text += '\n```'
    return text
}
