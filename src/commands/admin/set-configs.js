const setConfigValue = require('../../utils/set-config-value')
const CONSTANTS = require('../../constants/index')

const setAndSaveConfigValue = async (message, key, value) => {
    await setConfigValue(message, [key, value], true)
}
const checkMessage = async (message, args, hasPermissions, checkNaN) => {
    if (!hasPermissions) {
        message.channel.send('You do not have permission to do that.')
        return null
    }
    if (!args || args.length < 1) {
        message.channel.send('No arguments were provided.')
        return null
    }
    const [value] = args
    if (checkNaN) {
        if (Number.isNaN(Number(value))) {
            message.channel.send('Provided argument was not numeric, expected a numeric argument.')
            return null
        }
    }
    return value
}
const setRaceCategory = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, [args.join(' ')], hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.CATEGORY_KEY, value)
        return `The race category channel has been set to ${value}.`
    }
    return null
}
const setAsyncRaceCategory = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, [args.join(' ')], hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.ASYNC_CATEGORY_KEY, value)
        return `The async race category channel has been set to ${value}.`
    }
    return null
}
const setMainBotChannel = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, args, hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_CHANNEL, value)
        return `The primary bot channel has been set to ${value}.`
    }
    return null
}
const setMainBotRole = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, args, hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_ROLE, value)
        return `The primary bot role has been set to ${value}.`
    }
    return null
}
const setMaxVoiceForAdmin = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, args, hasPermissions, true)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.MAX_VOICE_ADMIN, value)
        return `The maximum number of voice channels to create before requiring admin permission is set to ${value}`
    }
    return null
}
const setMaxRacesForAdmin = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, args, hasPermissions, true)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.MAX_RACES_ADMIN, value)
        return `The maximum number of active races before requiring admin permission is set to ${value}`
    }
    return null
}
const setBotAdminRole = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, [args.join(' ')], hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.BOT_ADMIN_ROLE, value)
        return `Only users with the ${value} role will now have bot admin privileges.`
    }
    return null
}
const setRaceAlertChannel = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, [args.join(' ')], hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.RACE_ALERT_CHANNEL, value)
        return `The ${value} channel will now be sent race alerts.`
    }
    return null
}
const setRaceAlertRole = async (message, args, hasPermissions) => {
    const value = await checkMessage(message, [args.join(' ')], hasPermissions)
    if (value) {
        await setAndSaveConfigValue(message, CONSTANTS.SERVER_CONFIG.RACE_ALERT_ROLE, value)
        return `The ${value} role will be mentioned in race alerts.`
    }
    return null
}

module.exports = {
    setRaceCategory,
    setAsyncRaceCategory,
    setMaxVoiceForAdmin,
    setMaxRacesForAdmin,
    setBotAdminRole,
    setMainBotChannel,
    setMainBotRole,
    setRaceAlertChannel,
    setRaceAlertRole,
}
