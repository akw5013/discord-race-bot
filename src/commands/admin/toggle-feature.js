const getConfigValue = require('../../utils/get-config-value')
const CONSTANTS = require('../../constants')
const setConfigValue = require('../../utils/set-config-value')

module.exports = async (message, args, hasAdmin) => {
    if (!hasAdmin) {
        return null
    }
    if (!message.guild) {
        return null
    }
    if (!args || args.length === 0) {
        return 'You must provide an argument with the feature name. `!togglefeature featurename`'
    }
    const featureName = args[0].toLowerCase()
    if (!CONSTANTS.ACTIVE_FEATURES.FEATURES[featureName]) {
        return 'You must specify a valid feature name.  Use `!showfeatures` to show the current features for this server.'
    }
    const currentSetting = await getConfigValue(message, featureName, CONSTANTS.ACTIVE_FEATURES.FEATURES[featureName].defaultValue)
    const newSetting = !currentSetting
    await setConfigValue(message, [featureName, newSetting], true)
    return `${featureName} has been ${newSetting ? 'enabled' : 'disabled'}.`
}
