const moment = require('moment')
const getUserData = require('../../db/user/get-users')
const saveUserData = require('../../db/user/save-user')
const getNewUserDetails = require('../../utils/user/get-new-user-details')
const raceInfo = require('../race/race-data')

// const flatten = arr => arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten), [])

module.exports = async () => {
    const existingUsers = await getUserData()
    // console.log(existingUserIds)
    // First, find all unique users and create user information for them  (Don't delete any existing ones)
    // const allRaceEntrants = await raceInfo.finishedRaces().races.map(race => race.entrants)
    // const mergedList = flatten(allRaceEntrants)
    // const uniqueRaceEntrants = new Set(mergedList.map(entrant => entrant.id))

    //  const notInExisting = [...uniqueRaceEntrants].filter((ent) => !existingUserIds.find((i) => i === ent))
    //  console.log({notInExisting})
    // const notInRaces = existingUserIds.filter((id) => !mergedList.find((entrant) => entrant.id === id))
    // console.log({notInRaces})

    // console.log(allUsers)
    // Next, go through each finished race and tally stats for each user, saving along the way
    existingUsers.forEach((user) => {
        const racesForUser = raceInfo.finishedRaces.races.filter((race) => race.entrants.some((entrant) => entrant.id === user))
        const userNameInfo = existingUsers.find((usr) => usr.id === user.id)
        const userObj = { id: user, username: '' }
        if (userNameInfo) {
            userObj.username = userNameInfo.name
            userObj.streamInfo = userNameInfo.streamInfo
        } else {
            userObj.username = racesForUser[0].entrants.find((entrant) => entrant.id === user).name
        }
        const newStats = getNewUserDetails(userObj, null)
        let totalCookies = 5
        if (racesForUser && racesForUser.length > 0) {
            const totalRaces = racesForUser.length
            const totalFirst = racesForUser.reduce((total, race) => total + (race.entrants.find((entrant) => entrant.id === user).placement === 1 ? 1 : 0), 0)
            const totalSecond = racesForUser.reduce((total, race) => total + (race.entrants.find((entrant) => entrant.id === user).placement === 2 ? 1 : 0), 0)
            const totalThird = racesForUser.reduce((total, race) => total + (race.entrants.find((entrant) => entrant.id === user).placement === 3 ? 1 : 0), 0)
            const totalForfeit = racesForUser.reduce((total, race) => total + (race.entrants.find((entrant) => entrant.id === user).status === 'Forfeited' ? 1 : 0), 0)
            const completedRaces = racesForUser.filter((race) => race.entrants.find((entrant) => entrant.id === user).status !== 'Forfeited')
            const finishes = completedRaces.map((race) => ({ race: race.key, finish: race.entrants.find((entrant) => entrant.id === user).finish }))
            const pb = Math.min(...finishes.map((finish) => moment.duration(finish.finish, 'HH:mm:ss.SSS').asMilliseconds()))
            if (finishes.length > 0) {
                const pbRace = finishes.find((finish) => moment.duration(finish.finish, 'HH:mm:ss.SSS').asMilliseconds() === pb).race
                const pbFinish = finishes.find((finish) => moment.duration(finish.finish, 'HH:mm:ss.SSS').asMilliseconds() === pb).finish
                newStats.race_details.personal_best = pbFinish
                newStats.race_details.personal_best_race = pbRace
            }
            newStats.race_details.races_run = totalRaces
            newStats.race_details.races_first = totalFirst
            newStats.race_details.races_second = totalSecond
            newStats.race_details.races_third = totalThird
            newStats.race_details.races_forfeit = totalForfeit
            newStats.race_details.races_completed = completedRaces.map((race) => race.key)

            totalCookies += (3 * totalFirst)
            totalCookies += (2 * totalSecond)
            totalCookies += (1 * (totalRaces - totalFirst - totalSecond))
            totalCookies += (-3 * totalForfeit)
        }
        const seedsRolled = raceInfo.finishedRaces.races.reduce((total, race) => {
            if (race.metadata && race.metadata.Roller) {
                if (race.metadata.Roller === userObj.username) {
                    return total + 1
                }
            }
            return total
        }, 0)
        newStats.race_details.seeds_rolled = seedsRolled
        totalCookies += (3 * seedsRolled)
        newStats.cookies = totalCookies
        if (userObj.streamInfo) {
            newStats.streamInfo = userObj.streamInfo
        }
        saveUserData(newStats)
    })
    return 'Stats have been successfully updated.'
}
