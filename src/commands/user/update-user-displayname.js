const saveUserData = require('../../db/user/save-user')
const getUserDetails = require('../../utils/user/get-user-details')

const MAX_DISPLAY_NAME = 30
module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'A name must be provided.'
    }
    const newDisplayName = args.join(' ')
    if (newDisplayName.replace(' ', '') === '') {
        return 'A name must be provided.'
    }
    if (newDisplayName.length > MAX_DISPLAY_NAME) {
        return `Please choose a name less than ${MAX_DISPLAY_NAME} in length.`
    }
    const userDetails = await getUserDetails(message)
    await saveUserData(userDetails, { display_name: newDisplayName })
    return `From thenceforth, ${userDetails.name} shall now be called ${newDisplayName}`
}
