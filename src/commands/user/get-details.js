const findUser = require('../../utils/user/find-user')
const getTable = require('../../utils/table-layout')
const asyncForEach = require('../../utils/async-foreach')

module.exports = async (message, args) => {
    let theArgs = args
    if (!args || args.length === 0) {
        theArgs = [message.author.username]
    }
    const data = []
    await asyncForEach(theArgs.filter((arg) => arg !== ''), async (username) => {
        const user = await findUser(username, message.guild)
        if (user) {
            data.push({
                name: user.display_name || user.name,
                races: user.race_details.races_run,
                firsts: user.race_details.races_first,
                seconds: user.race_details.races_second,
                thirds: user.race_details.races_third,
                stream: user.streamInfo,
                cookies: user.cookies,
                seeds_rolled: user.race_details.seeds_rolled,
                badges: (user.badges && user.badges.map((b) => b.name).join(', ')) || '',
            })
        }
    })
    if (data.length === 0) {
        return 'No users found.'
    }
    const headers = ['Name', 'Races', 'Firsts', 'Seconds', 'Thirds', 'Stream', 'Cookies', 'Seeds Rolled', 'Badges']
    const table = getTable(headers, data)
    return `\`\`\`\n${table}\n\`\`\``
}
