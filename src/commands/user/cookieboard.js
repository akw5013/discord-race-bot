const getUserData = require('../../db/user/get-users')
const getTable = require('../../utils/table-layout')
const getUserDisplayName = require('../../utils/user/get-user-display-name')

const DEFAULT_AMOUNT = 3
const MAX_AMOUNT = 20
const getCookieboard = async (_message, args) => {
    const users = await getUserData()
    let topAmount = DEFAULT_AMOUNT
    if (args && args.length > 0) {
        topAmount = Number(args[0])
        if (topAmount > MAX_AMOUNT) {
            topAmount = MAX_AMOUNT
        }
        if (topAmount < 3) {
            topAmount = DEFAULT_AMOUNT
        }
        if (Number.isNaN(topAmount)) {
            topAmount = DEFAULT_AMOUNT
        }
    }
    const topThree = users.sort((a, b) => b.cookies - a.cookies).slice(0, topAmount).map((user) => ({ name: getUserDisplayName(user), cookies: user.cookies }))
    const headers = ['User', 'Cookies']
    const data = getTable(headers, topThree)
    return `\`\`\`\nDisplaying The Top ${topAmount} Owners of Cookies.\n${data}\n\`\`\``
}
module.exports = getCookieboard
