const sendMessage = require('../../utils/send-message');

module.exports = async (message) => {
    const flip = Math.random();

    const result = flip < 0.5 ? "heads" : "tails";
    
    sendMessage(message.channel, "Flipping coin...");
    setTimeout(() => {
        sendMessage(message.channel, `The result is ${result}!`)
    }, 2000);
}