const buyReward = require('./buy-reward')
const showRewards = require('./show-rewards')
const showFeatured = require('./show-featured')
const showBadges = require('./show-badges')

module.exports = {
    '!showrewards': showRewards,
    '!rewards': showRewards,
    '!buyreward': buyReward,
    '!featured': showFeatured,
    '!showfeatured': showFeatured,
    '!badges': showBadges,
    '!showbadges': showBadges,
}
