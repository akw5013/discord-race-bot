const { rewards } = require('../../../data/rewards.json')
const buildTable = require('../../utils/table-layout')

module.exports = async () => {
    const headers = ['Name', 'Description', 'Cost', 'Duration']
    const data = rewards.filter((reward) => reward.enabled)
    return `\`\`\`\n${buildTable(headers, data)}\n\`\`\``
}
