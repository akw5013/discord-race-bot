const moment = require('moment')
const saveUser = require('../../db/user/save-user')

module.exports = async (user, reward) => {
    if (!user || !reward) {
        return null
    }
    const updatedUser = user
    const updatedReward = reward
    if (!updatedUser.rewards) {
        updatedUser.rewards = []
    }
    if (!updatedUser.badges) {
        updatedUser.badges = []
    }
    const now = moment()
    updatedReward.purchased = now.toISOString()
    if (updatedReward.type === 'Badge') {
        updatedUser.badges.push(updatedReward)
    } else if (updatedReward.type === 'Reward') {
        const [time, unit] = updatedReward.duration.split(' ')
        updatedReward.expiration = now.add(time, unit)
    }
    updatedUser.rewards.push(updatedReward)
    await saveUser(user, { rewards: updatedUser.rewards, badges: updatedUser.badges })
    return null
}
