const utils = require('./race-utils')
const getUserById = require('../../db/user/get-user-by-id')
const getTable = require('../../utils/table-layout')
const CONSTANTS = require('../../constants')
const sendDirectMessage = require('../../utils/send-direct-message')

const listEntrants = async (message) => {
    const race = await utils.getCurrentRace(message.channel.name)
    if (race) {
        const isRaceFinished = race.status === CONSTANTS.RACE_STATUS.FINISHED
        const isRaceAsync = !!race.async
        const shouldHidePlacements = isRaceAsync && !isRaceFinished
        const entrantDetails = race.entrants.filter((en) => en).map(async (entrant) => {
            const { name, status } = entrant
            let { team, placement, finish } = entrant
            const userInfo = await getUserById(entrant.id)
            const stream = userInfo ? userInfo.streamInfo : '-'
            if (team === name) { team = '-' }
            if (!placement || shouldHidePlacements) { placement = '-' }
            if (!finish || shouldHidePlacements) { finish = '-' }
            return {
                name, status, team, placement, finish, stream,
            }
        })
        const finalDetails = await Promise.all(entrantDetails)
        const headers = ['Name', 'Status', 'Team', 'Placement', 'Finish', 'Stream']
        const shouldDMMessage = !isRaceFinished && race.official
        const returnMessage = `\`\`\`\n${getTable(headers, finalDetails)}\n\`\`\``
        if (shouldDMMessage) {
            message.delete()
            sendDirectMessage(message.author, returnMessage)
            return null
        }
        return returnMessage
    }
    return null
}
module.exports = listEntrants
