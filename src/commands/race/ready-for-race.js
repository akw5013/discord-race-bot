const utils = require('./race-utils')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const readyForRace = async (message) => {
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const entrant = utils.findEntrant(currentRace, message.author)
    if (!entrant) {
        return `${message.author.username} is not currently in this race.`
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.READY) {
        return `${message.author.username} is already ready.`
    }
    if (!isRaceStartable(currentRace)) {
        return `Cannot ready up for a race in the ${currentRace.status} status.`
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.READY
    saveEntrant(currentRace, entrant)
    const numberUnready = currentRace.entrants.filter((ent) => ent.status !== CONSTANTS.ENTRANT_STATUS.READY).length
    await utils.checkForRaceStart(message, currentRace)
    updateRaceMessages(message.client, currentRace)
    return `${message.author.username} is ready! (${numberUnready} left to ready)`
}
module.exports = readyForRace
