const utils = require('./race-utils')
const saveRace = require('../../db/save-race')
const securityUtils = require('../../utils/security/security-utils')

module.exports = async (message, args, hasPermissions) => {
    if (!hasPermissions) {
        return 'You do not have permissions to do that.'
    }
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) {
        return 'There is no race in this channel.'
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }

    saveRace(race, { official: false, shouldDelete: true })

    return 'The race is now unofficial.'
}
