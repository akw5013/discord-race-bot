const utils = require('./race-utils')
const CONSTANTS = require('../../constants')
const saveEntrant = require('../../db/race/save-entrant')
const deleteFinisher = require('../../db/race/remove-finisher-from-race')
const saveFinisher = require('../../db/race/save-finisher')
const securityUtils = require('../../utils/security/security-utils')

const unfinishRace = async (message) => {
    const user = message.author
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (race.status !== CONSTANTS.RACE_STATUS.RUNNING) {
        return 'You cannot unfinish this race, as it has not started or has already finished.'
    }
    const entrant = utils.findEntrant(race, user)
    if (!entrant) {
        return 'You are not currently in this race.'
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        return 'You cannot unfinish this race. Please use !unforfeit instead.'
    }
    if (entrant.status !== CONSTANTS.ENTRANT_STATUS.FINISHED) {
        return 'You have not finished this race.'
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.RUNNING
    entrant.finishTime = null
    entrant.finish = null
    // Reorder all finishers that were after this one
    // fix entrant.placement
    // fix race.finishers
    race.finishers.splice(race.finishers.findIndex((finisher) => finisher.id === entrant.id), 1)
    const finishersToAdjust = race.finishers.filter((finisher) => finisher.placement > entrant.placement)
    finishersToAdjust.forEach((finisher) => {
        const theFinisher = finisher
        theFinisher.placement -= 1
        saveFinisher(race, finisher)
    })
    entrant.placement = null
    saveEntrant(race, entrant)
    deleteFinisher(race, entrant)
    return `${user.username} has cancelled their finish!`
}
module.exports = unfinishRace
