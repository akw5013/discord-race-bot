const randomstring = require('randomstring')
const moment = require('moment')
const logger = require('../../utils/logger')
const getConfigValue = require('../../utils/get-config-value')
const raceAlert = require('../../utils/race/create-race-alert')
const getRaceStatusMessage = require('../../utils/race/get-race-status-message')
const createRaceInDb = require('../../db/race/create-race')
const CONSTANTS = require('../../constants')
const createChannel = require('../../utils/create-channel')
const sendIntroMessages = require('../../utils/race/send-race-intro-messages')
const getEventByName = require('../../db/event/get-event-by-name')
const saveRaceToEvent = require('../../db/event/save-race-to-event')

const randomlength = 6

const createRace = async (message, args, isAdmin) => {
    const botChannel = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_CHANNEL, CONSTANTS.CONFIG.DEFAULT_BOT_CHANNEL)
    if (message.channel.name !== botChannel) { return null }
    if (!message.guild) { return null } // Can't start races from DM
    const isAsyncRace = args.includes('--async')
    const isEventRace = args.includes('--event')
    const isRestricted = args.includes('--restrict')
    const isOfficial = args.includes('--official')
    const isAsyncEnabled = await getConfigValue(message, CONSTANTS.ACTIVE_FEATURES.FEATURES.asyncracing.name, CONSTANTS.ACTIVE_FEATURES.FEATURES.defaultValue)
    if (isAsyncRace && !isAsyncEnabled) {
        return 'Async races have been disabled on this server.'
    }
    let event = null
    if (isEventRace) {
        event = await getEventByName(args[args.indexOf('--event') + 1])
        if (!event) {
            return `No event with name ${event} was found. Unable to create event linked race.`
        }
        if (!event.admins.some((ev) => ev.id === message.author.id)) {
            return 'You do not have the ability to create a race for this event. Please contact an event admin to create it for you.'
        }
        if (event.finishTime) {
            return 'This event has already concluded.'
        }
        if (event.server !== 'ALL' && event.server !== message.guild.id) {
            return `You may not create a race for ${event.name} on this server. Races for this event are only allowed on server ${event.server}.`
        }
    }
    let raceCategory = await getConfigValue(message, isAsyncRace ? CONSTANTS.SERVER_CONFIG.ASYNC_CATEGORY_KEY : CONSTANTS.SERVER_CONFIG.CATEGORY_KEY, CONSTANTS.CONFIG.DEFAULT_RACE_CATEGORY) // eslint-disable-line max-len
    if (isEventRace) {
        const eventCategory = message.guild.channels.cache.find((channel) => channel.name === event.name)
        if (eventCategory) {
            raceCategory = eventCategory.name
        }
    }
    const activeRaceChannels = message.guild.channels.cache.filter((channel) => channel.parent
        && channel.parent.name === raceCategory
        && channel.type === 'GUILD_TEXT' && !channel.name.endsWith('spoilers'))
    const totalChannelsInCategory = message.guild.channels.cache.filter((channel) => channel.parent && channel.parent.name === raceCategory).size
    if (totalChannelsInCategory >= 48) {
        return 'The maximum allowable number of channels by Discord has been reached.  Please close some channels and try again.'
    }
    logger.logDebug(`There are ${activeRaceChannels.size} active race channels`)
    if (activeRaceChannels.size >= await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAX_RACES_ADMIN, CONSTANTS.CONFIG.DEFAULT_MAX_RACES)) {
        if (!isAdmin) {
            return 'The maximum number of races that can be created by a non-admin has been reached.  Please ask an admin to create the race room.'
        }
    }
    if (!args || args.length < 1) {
        return 'Invalid command. Command usage: `!startrace gamename`.'
    }
    if (!isAdmin && isOfficial) {
        return 'Only an admin can make official races.'
    }
    const gameName = args[0].toLowerCase()
    const isAllowedGame = CONSTANTS.ALLOWED_GAMES.GAMES.includes(gameName)
    if (!isAllowedGame) {
        return `Invalid game name entered.  Please select from: ${CONSTANTS.ALLOWED_GAMES.GAMES.join(', ')}`
    }
    let gameMode = 'ffa'
    if (args.length > 1 && args[1]) {
        if (CONSTANTS.ALLOWED_GAME_MODES.MODES.includes(args[1].toLowerCase())) {
            [, gameMode] = args
        }
    }
    const channelName = (`${gameName}-${randomstring.generate(randomlength)}${isAsyncRace ? '-async' : ''}`).toLowerCase()
    const adminRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.BOT_ADMIN_ROLE, CONSTANTS.CONFIG.DEFAULT_ADMIN_ROLE)
    const botRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_ROLE, message.client.user.username)
    const raceChannel = await createChannel(message.guild, channelName, 'GUILD_TEXT', raceCategory, adminRole, botRole)
    const restrictedRoleName = isRestricted ? args[args.indexOf('--restrict') + 1] : ''
    const race = {
        key: channelName,
        guild: { id: message.guild.id, name: message.guild.name },
        game: gameName,
        type: args,
        mode: gameMode,
        shouldDelete: !isOfficial,
        status: CONSTANTS.RACE_STATUS.OPEN,
        entrants: [],
        finishers: [],
        created: moment().toISOString(true),
        async: isAsyncRace,
        spoilerChannels: [],
        creator: { name: message.author.username, id: message.author.id },
        official: isOfficial,
    }
    if (restrictedRoleName) {
        race.restrictedToRole = restrictedRoleName
    }
    if (isAsyncRace) {
        const spoilerChannel = await createChannel(message.guild, `${channelName}-spoilers`, 'GUILD_TEXT', raceCategory, adminRole, botRole)
        race.spoilerChannels.push(spoilerChannel.id)
    }
    if (event) {
        race.event = { id: event.id, name: event.name }
    }
    const raceMessage = `A${isOfficial ? 'n official' : ''} room has been created for ${gameName} (${gameMode}), your room is ${channelName}.  Please type \`!join ${channelName}\` in the ${botChannel} channel to join the race and gain access to the lobby.` // eslint-disable-line max-len
    const shouldPingPeople = !isAsyncRace && !isEventRace && !isRestricted
    const alertMsg = await raceAlert(message, getRaceStatusMessage(race), shouldPingPeople)
    if (alertMsg) {
        race.messages = [{ guildId: alertMsg.guild.id, channelId: alertMsg.channel.id, messageId: alertMsg.id }]
    }
    const savedRace = await createRaceInDb(race)
    if (!savedRace) {
        return `There was an error creating the race. Please get an admin to delete the ${channelName} channel and try again.`
    }
    if (event) {
        saveRaceToEvent(event, { id: savedRace.id, key: savedRace.key })
    }
    sendIntroMessages(race, raceChannel)
    return raceMessage
}

module.exports = createRace
