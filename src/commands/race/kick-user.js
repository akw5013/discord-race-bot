const utils = require('./race-utils')
const findUser = require('../../utils/user/find-user')
const deleteEntrant = require('../../db/race/delete-entrant')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const securityUtils = require('../../utils/security/security-utils')

module.exports = async (message, args, hasPermissions) => {
    if (!hasPermissions) {
        return 'You do not have permissions to do that.'
    }
    if (!args || args.length === 0) {
        return 'You must specify a user.'
    }
    const [userName] = args
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) {
        return 'There is no race in this channel.'
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    const user = await findUser(userName, message.guild)
    const entrant = await utils.findEntrant(race, user)
    if (!entrant) {
        return `${userName} is not currently in this race.`
    }
    await deleteEntrant(race, entrant)
    utils.checkForRaceStart(message, race)
    updateRaceMessages(message.client, race)
    return `${message.author.username} has kicked ${userName} out of the race.`
}
