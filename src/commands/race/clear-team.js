const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const CONSTANTS = require('../../constants')

module.exports = async (message) => {
    if (!message.guild) { return null }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    if (!currentRace) { return null }
    const entrant = utils.findEntrant(currentRace, message.author)
    let result = null
    if (entrant && entrant.status !== CONSTANTS.ENTRANT_STATUS.READY && (isRaceStartable(currentRace) || currentRace.async)) {
        entrant.team = entrant.name
        saveEntrant(currentRace, entrant)
        result = `${entrant.name}'s team has been reset.`
    } else {
        result = `${message.author.username} is not currently allowed to change teams`
    }
    return result
}
