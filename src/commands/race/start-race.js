const NanoTimer = require('nanotimer')
const moment = require('moment')
const CONSTANTS = require('../../constants')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const saveRace = require('../../db/save-race')
const findUser = require('../../utils/user/find-user')
const saveUser = require('../../db/user/save-user')
const saveEntrant = require('../../db/race/save-entrant')
const updateCookies = require('../../db/user/update-cookies')

const countDown = (channel, counter) => {
    const theCounter = counter
    if (theCounter.time < 4 && theCounter.time > 0) {
        channel.send(`Race is starting in ${theCounter.time}...`)
    }
    theCounter.time -= 1
}

const commence = async (timer, channel, race) => {
    const theRace = race
    theRace.status = CONSTANTS.RACE_STATUS.RUNNING
    theRace.startTime = moment().toISOString(true)
    channel.send('The race has started! Good luck and have fun!')
    timer.clearInterval()
    const peopleWhoWagered = theRace.entrants.filter((en) => en.wager && en.wager > 0).length
    const shouldEnableWager = peopleWhoWagered >= 3
    if (!shouldEnableWager && peopleWhoWagered > 0) {
        channel.send('Not enough people wagered cookies, all wagers have been cancelled.')
    }
    const totalWagered = shouldEnableWager ? theRace.entrants.reduce((total, entrant) => total + (entrant.wager || 0), 0) : 0
    await saveRace(theRace, { status: theRace.status, startTime: theRace.startTime, totalWagered })
    theRace.entrants.forEach(async (entrant) => {
        const theEntrant = entrant
        theEntrant.status = CONSTANTS.ENTRANT_STATUS.RUNNING
        if (shouldEnableWager && theEntrant.wager) {
            const userInfo = await findUser(theEntrant.name, channel.guild)
            if (userInfo) {
                updateCookies(userInfo, -1 * theEntrant.wager, theRace.key, `Wagered cookies on race ${theRace.key}`)
            }
        } else {
            theEntrant.wager = 0
        }
        saveEntrant(theRace, theEntrant)
    })
    await updateRaceMessages(channel.client, theRace)
}

const startRace = async (channel, race) => {
    const theRace = race
    const timer = new NanoTimer()
    const count = { time: 9 }
    setImmediate(async () => {
        theRace.status = CONSTANTS.RACE_STATUS.STARTING
        updateRaceMessages(channel.client, theRace)
        saveRace(theRace, { status: theRace.status })
        channel.send('All racers have readied up.  Race starting in 10 seconds...')
        timer.setInterval(countDown, [channel, count], '1s')
        timer.setTimeout(commence, [timer, channel, race], '10s')
        if (theRace.metadata && theRace.metadata.Roller) {
            const userInfo = await findUser(theRace.metadata.Roller, channel.guild)
            if (userInfo) {
                updateCookies(userInfo, 3, 'Rolling a seed', `Rolled seed for race ${theRace.id}/${theRace.key}`)
                userInfo.race_details.seeds_rolled += 1
                await saveUser(userInfo, { race_details: { seeds_rolled: userInfo.race_details.seeds_rolled } })
            }
        }
    })
}

module.exports = startRace
