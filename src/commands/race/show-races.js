const racedata = require('./race-data')
const getConfigValue = require('../../utils/get-config-value')
const sendDirectMessage = require('../../utils/send-direct-message')
const displayTable = require('../../utils/table-layout')
const CONSTANTS = require('../../constants')
const sortRaces = require('./sort-races')

const showRaces = async (message) => {
    if (message.channel.name !== await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_CHANNEL, CONSTANTS.CONFIG.BOT_CHANNEL)) { return }
    const allRaces = await racedata.currentRaces()
    let filteredRaces = allRaces
    if (message.guild) {
        filteredRaces = filteredRaces.filter((race) => {
            if (!race.guild || !race.guild.id) {
                return false
            }
            return race.guild.id === message.guild.id
        })
    }
    let textValue = '```\n'
    if (filteredRaces.length > 20) {
        filteredRaces = filteredRaces.sort(sortRaces)
        filteredRaces.splice(0, 20)
        textValue += 'Only the most recent 20 results are shown.\n'
    }
    const headers = ['Name', 'Server', 'Status', 'Game', 'Create Time', 'Entrants', 'Async?']
    const rows = filteredRaces.map((race) => ({
        name: race.key,
        server: (race.guild && race.guild.name) || 'Unknown',
        status: race.status,
        game: race.type[0],
        created: race.created,
        entrants: race.entrants.length,
        async: race.async,
    }))
    textValue += displayTable(headers, rows)
    textValue += '```'
    sendDirectMessage(message.author, textValue)
    if (message.guild) {
        message.delete()
    }
}
module.exports = showRaces
