const utils = require('./race-utils')
const CONSTANTS = require('../../constants')
const getUserDetails = require('../../utils/user/get-user-details')
const getUserDisplayName = require('../../utils/user/get-user-display-name')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const sendDirectMessage = require('../../utils/send-direct-message')
const getActiveRaceForUser = require('../../utils/user/get-active-race-for-user')
const addEntrantToRace = require('../../db/race/add-entrant-to-race')
const securityUtils = require('../../utils/security/security-utils')
const isRaceEditable = require('../../utils/race/is-race-editable')

const sendDMAndDeleteMessage = (messageToDelete, messageToSend) => {
    sendDirectMessage(messageToDelete.author, messageToSend)
    messageToDelete.delete()
}
const canJoinRace = (race, message, isAdmin) => {
    if (!race) {
        return { canJoin: false, reason: 'Race does not exist. Did you forget a race name parameter?' }
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return {
            canJoin: false,
            reason: securityResult,
        }
    }
    if (!isRaceEditable(race) && !race.async) {
        return {
            canJoin: false,
            reason: `You are unable to join a race in the ${race.status} state.  If you would like to see the details, please use \`!watch raceName\``,
        }
    }
    if (race.status === CONSTANTS.RACE_STATUS.FINISHED) {
        return { canJoin: false, reason: 'That race has already been completed.' }
    }
    if (race.restrictedToRole && !isAdmin) {
        const guildRole = message.guild.roles.cache.find((role) => role.name === race.restrictedToRole)
        if (!guildRole) {
            return { canJoin: false, reason: `The race is restricted to the ${race.restrictedToRole} role, but it cannot be found.` }
        }
        if (!message.member) {
            return { canJoin: false, reason: 'You are not a member of the server this race is on.' }
        }
        if (!message.member.roles.cache.get(guildRole.id)) {
            return { canJoin: false, reason: `You do not have the ${race.restrictedToRole} role.` }
        }
    }
    return { canJoin: true, reason: null }
}
module.exports = async (message, args, hasAdmin) => {
    const activeRaceForUser = await getActiveRaceForUser(message.author)
    let currentRace = null
    let { channel } = message
    if (args && args.length > 0) {
        const [raceName] = args
        currentRace = await utils.getCurrentRace(raceName)
        const raceChannel = message.guild.channels.cache.find((guildChannel) => guildChannel.name === raceName && guildChannel.type === 'GUILD_TEXT')
        if (!raceChannel) {
            sendDMAndDeleteMessage(message, `Channel ${raceName} does not exist.  This may be due to a bot error, please inform an admin.`)
            return null
        }
        channel = raceChannel
    } else {
        currentRace = await utils.getCurrentRace(message.channel.name)
    }
    const joinDetails = canJoinRace(currentRace, message, hasAdmin)
    if (!joinDetails.canJoin) {
        sendDMAndDeleteMessage(message, joinDetails.reason)
        return null
    }
    if (activeRaceForUser && !currentRace.async) {
        sendDMAndDeleteMessage(message, `You cannot join another non-async race. Please finish or forfeit from ${activeRaceForUser.key} before joining another race.`)
        return null
    }
    channel.permissionOverwrites.create(message.author, { VIEW_CHANNEL: true, SEND_MESSAGES: true })
    const entrantStatus = currentRace.async ? CONSTANTS.ENTRANT_STATUS.RUNNING : CONSTANTS.ENTRANT_STATUS.WAITING
    const entrant = await utils.findEntrant(currentRace, message.author)
    const userInfo = await getUserDetails(message)
    const userDisplayName = await getUserDisplayName(userInfo, message.guild)
    if (entrant) {
        sendDMAndDeleteMessage(message, `${userDisplayName} has already joined this race.`)
        return null
    }
    setImmediate(() => message.delete())
    await addEntrantToRace(currentRace, {
        name: userDisplayName, status: entrantStatus, id: message.author.id, team: userDisplayName,
    })
    channel.send(`${userDisplayName} has joined the race`)
    await updateRaceMessages(message.client, currentRace)
    return null
}
