const fetch = require('node-fetch')
const moment = require('moment')
const getUserById = require('../../db/user/get-user-by-id')
const utils = require('./race-utils')
const randomNum = require('../../utils/get-random-number')
const logger = require('../../utils/logger')

const twitchApiBase = 'https://api.twitch.tv/helix/'
const twitchOAuthUrl = 'https://id.twitch.tv/oauth2/token?'
const twitchClientKey = process.env.TWITCH_CLIENT_ID
const twitchClientSecret = process.env.TWITCH_CLIENT_SECRET

let oauthToken
let oauthExpiration

const MULTIBASE = 'https://kadgar.net/live'

const getOAuthToken = async () => {
    if (!oauthToken || moment().isAfter(oauthExpiration)) {
        const twitchUrl = `${twitchOAuthUrl}&client_id=${twitchClientKey}&client_secret=${twitchClientSecret}&grant_type=client_credentials`
        const oauthResponse = await fetch(twitchUrl, { method: 'POST' })
        const oauthData = await oauthResponse.json()
        oauthToken = oauthData.access_token
        oauthExpiration = moment().add(oauthData.expires_in - 60, 'seconds')
    }
    return oauthToken
}

const isStreamLive = async (url) => {
    const userName = url.replace('https://twitch.tv/', '')
    const apiUrl = `${twitchApiBase}streams?user_login=${userName}`
    return fetch(apiUrl, {
        headers: {
            'Client-ID': twitchClientKey,
            Authorization: `Bearer ${await getOAuthToken()}`,
        },
    }).then((response) => response.json())
        .catch((e) => {
            logger.logError(e)
            return false
        })
        .then((json) => json.data && json.data.length > 0 && json.data[0].type === 'live')
}
const getMultistreamLink = async (message, args) => {
    let raceKey = message.channel.name
    if (args && args.length > 0) {
        raceKey = args[0]
    }
    const race = await utils.getCurrentRace(raceKey)
    if (!race) {
        return null
    }
    const limitResults = args.length > 1
    const resultLimit = Number(limitResults ? args[1] : -1)
    const teamLinks = { }
    const streams = race.entrants.map(async (entrant) => {
        const userInfo = await getUserById(entrant.id)
        let streamName = ''
        if (userInfo && userInfo.streamInfo) {
            const isLive = await isStreamLive(userInfo.streamInfo)
            if (!isLive) {
                return ''
            }
            streamName = userInfo.streamInfo.replace('https://twitch.tv/', '')
            if (entrant.name !== entrant.team) {
                if (!teamLinks[entrant.team]) {
                    teamLinks[entrant.team] = [userInfo.streamInfo.replace('https://twitch.tv/', '')]
                } else {
                    teamLinks[entrant.team].push(streamName)
                }
            }
        }
        return streamName
    })
    const theStreams = await Promise.all([...streams])
    const streamStrings = theStreams.filter((strm) => strm !== '').join('/')
    if (streamStrings === '') {
        return "There are no live streams currently set up with this race's entrants."
    }
    let multiLinks = '```\n'
    if (!limitResults || resultLimit >= theStreams.length) {
        Object.keys(teamLinks).forEach((team) => {
            multiLinks += `${team}: ${MULTIBASE}/${teamLinks[team].join('/')}\n`
        })
        multiLinks += `Everyone: ${MULTIBASE}/${streamStrings}`
        multiLinks += '\n```'
        return multiLinks
    }
    const alreadyUsed = []
    const pickedStreams = []
    for (let i = 0; i < resultLimit; i += 1) {
        let randStream = randomNum(0, theStreams.length)
        while (alreadyUsed.includes(randStream)) {
            randStream = randomNum(0, theStreams.length)
        }
        alreadyUsed.push(randStream)
        pickedStreams.push(theStreams[randStream])
    }
    multiLinks += pickedStreams.join('/')
    multiLinks += '\n```'
    return multiLinks
}
module.exports = getMultistreamLink
