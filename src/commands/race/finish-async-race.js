const moment = require('moment')
const securityUtils = require('../../utils/security/security-utils')
const getRace = require('../../db/race/get-race-by-key')
const saveRace = require('../../db/save-race')
const CONSTANTS = require('../../constants')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const buildRaceDetails = require('../../utils/race/build-race-details')
const sendMessage = require('../../utils/send-message')
const deleteRaceMessages = require('../../utils/race/delete-race-messages')
const sendEntrantDMs = require('../../utils/race/send-dm-to-entrants')
const asyncForEach = require('../../utils/async-foreach')
const updateUserDetails = require('../../utils/user/update-user-race-details')
const getUserById = require('../../db/user/get-user-by-id')
const findUser = require('../../utils/user/find-user')
const saveUser = require('../../db/user/save-user')
const badgeProcessor = require('../../utils/badges/badge-processor')
const processEventRace = require('../../db/event/process-event-race')

module.exports = async (message, args, hasAdmin) => {
    const race = await getRace(message.channel.name)
    if (!race) { return 'There is no race in this room.' }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (!race.async) { return 'This race is not async and cannot be finished as one.' }
    if (!securityUtils.isSentByCreatorOrAdmin(message, race, hasAdmin)) {
        return `You do not have permission to finish the race. Only ${race.creator.name} or an admin may finish this race.`
    }
    race.totalWagered = 0
    race.status = CONSTANTS.RACE_STATUS.FINISHED
    // Update all of the entrant placements
    const finishers = race.entrants.filter((entrant) => entrant.finish && entrant.finish !== CONSTANTS.ENTRANT_STATUS.FORFEIT)
    race.finishers = finishers
    race.entrants.forEach((entrant) => {
        const updatedEntrant = entrant
        const sortedFinishers = race.finishers.sort((a, b) => moment.duration(a.finish).asMilliseconds() - moment.duration(b.finish).asMilliseconds())
        const index = sortedFinishers.indexOf(entrant)
        if (index >= 0) {
            updatedEntrant.placement = index + 1
        } else {
            updatedEntrant.placement = race.entrants.length
        }
    })
    if (race.metadata && race.metadata.Roller) {
        const userInfo = await findUser(race.metadata.Roller, message.channel.guild)
        if (userInfo) {
            userInfo.race_details.seeds_rolled += 1
            userInfo.cookies += 3
            await saveUser(userInfo, { cookies: userInfo.cookies, race_details: { seeds_rolled: userInfo.race_details.seeds_rolled } })
        }
    }
    const newBadges = []
    const errors = []
    await asyncForEach(race.finishers, async (finisher) => {
        const { placement, finish } = finisher
        const userInfo = await getUserById(finisher.id)
        const error = await updateUserDetails(userInfo, {
            placement,
            key: race.key,
            time: finish,
            totalWagered: 0,
        })
        if (error) {
            errors.push(error)
            return
        }
        const badgesForUser = badgeProcessor(null, race, userInfo)
        if (badgesForUser && badgesForUser.length > 0) {
            newBadges.push({ name: finisher.name, id: finisher.id, badges: badgesForUser })
            if (!userInfo.badges) { userInfo.badges = [] }
            userInfo.badges.push(...badgesForUser.map((badge) => ({ name: badge.name, description: badge.description, earnedFrom: badge.earnedFrom })))
            await saveUser(userInfo, { badges: userInfo.badges })
        }
    })
    await updateRaceMessages(message.client, race)
    race.status = CONSTANTS.RACE_STATUS.FINISHED
    race.finishTime = moment().toISOString(true)
    await saveRace(race, {
        status: CONSTANTS.RACE_STATUS.FINISHED,
        entrants: race.entrants,
        finishers: race.finishers,
        finishTime: race.finishTime,
    })
    if (race.event) {
        processEventRace(race.event.id, race)
    }
    const { guild, channel } = message
    const details = buildRaceDetails(race)
    await sendMessage(channel, details)
    setImmediate(() => {
        channel.send(`This room and associated voice channels will be closed in ${CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MINUTES} minutes.`)
        if (errors.length) {
            channel.send(`The following errors occurred while processing race finish:\n${errors.join('\n')}`)
        }
        if (newBadges.length > 0) {
            channel.send(`\`\`\`The following users have earned new badges!!!\n${newBadges.map((nb) => `${nb.name}: ${nb.badges.map((bd) => bd.name).join(', ')}`).join('\n')}\n\`\`\``)
        }
        setTimeout(() => {
            const textChannel = guild.channels.cache.find((ch) => ch.name === race.key)
            const voiceChannels = guild.channels.cache.filter((ch) => ch.name.includes(race.key) && ch.type === 'voice')
            const spoilerChannels = guild.channels.cache.filter((ch) => race.spoilerChannels.indexOf(ch.id) >= 0)
            if (textChannel) {
                textChannel.delete()
            }
            if (voiceChannels) {
                voiceChannels.forEach((vc) => vc.delete())
            }
            if (spoilerChannels) {
                spoilerChannels.forEach((sc) => sc.delete())
            }
            deleteRaceMessages(guild.client, race)
        }, CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MILLIS)
    })
    sendEntrantDMs(message.client, race, details)
    return 'The race has been finished.'
}
