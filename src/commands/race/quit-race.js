const utils = require('./race-utils')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const deleteEntrant = require('../../db/race/delete-entrant')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')
const CONSTANTS = require('../../constants')

const quitRace = async (message) => {
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const entrant = utils.findEntrant(currentRace, message.author)
    if (!entrant) {
        return `${message.author.username} is not currently in this race.`
    }
    if (!isRaceStartable(currentRace)) {
        return `${message.author.username} cannot quit the race after it's been started.  Please \`!forfeit\` instead`
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FINISHED) {
        return `${message.author.username} has already finished the race.`
    }
    currentRace.entrants.splice(currentRace.entrants.indexOf(entrant), 1)
    deleteEntrant(currentRace, entrant)
    utils.checkForRaceStart(message, currentRace)
    updateRaceMessages(message.client, currentRace)
    return `${message.author.username} has quit the race.`
}
module.exports = quitRace
