const utils = require('./race-utils')
const sendDirectMessage = require('../../utils/send-direct-message')

const watchRace = async (message, args) => {
    if (!args || args.length === 0) {
        return 'Invalid command.  Did you forget to include the race name? Usage: `!watch raceName`'
    }
    if (!message.guild) {
        sendDirectMessage(message.author, 'You must use this command on the server you wish to watch the race of.')
        message.delete()
        return null
    }
    const [raceName] = args
    const currentRace = await utils.getCurrentRace(raceName)
    if (!currentRace) {
        sendDirectMessage(message.author, `No race with name ${raceName} was found. Please check your spelling and try again.`)
        message.delete()
        return null
    }
    const raceChannel = message.guild.channels.cache.find((channel) => channel.name === raceName && channel.type === 'GUILD_TEXT')
    if (!raceChannel) {
        sendDirectMessage(message.author, `No race channel for ${raceName} was found.  This may be a bot error, please contact an admin.`)
        message.delete()
        return null
    }
    raceChannel.permissionOverwrites.create(message.author, { VIEW_CHANNEL: true, SEND_MESSAGES: true })
    setImmediate(() => {
        message.delete()
    })
    return null
}
module.exports = watchRace
