const checkMessage = require('../../utils/check-message')
const raceUtils = require('./race-utils')
const saveRace = require('../../db/save-race')
const securityUtils = require('../../utils/security/security-utils')

const MAX_KEY_LENGTH = 75
const MAX_VALUE_LENGTH = 1024

const notAllowedCharacters = /[^ -~]/g

module.exports = async (message, args, hasAdmin) => {
    if (!checkMessage(message, { checkRace: true, checkArgs: true })) { return null }
    if (!args || args.length < 2 || args.length % 2 !== 0) {
        return 'Invalid arguments specified.'
    }
    const currentRace = await raceUtils.getCurrentRace(message.channel.name)
    if (!securityUtils.isSentByCreatorOrAdmin(message, currentRace, hasAdmin)) { return null }
    // No need to check currentRace since it's already checked for in checkMessage
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const metaPairs = args.reduce((ary, arg, indx) => {
        if (indx % 2 === 0) {
            ary.push([arg])
        } else {
            ary[ary.length - 1].push(arg)
        }
        return ary
    }, [])
    if (!currentRace.metadata) {
        currentRace.metadata = { }
    }
    const statusMessages = metaPairs.map((metaPair) => {
        const [metaType, metaValue] = metaPair
        const scrubbedMetaType = metaType.replace(/\s/g, '').replace(notAllowedCharacters, '').replace(/[:<>]/g, '').trim()
        const scrubbedMetaValue = metaValue.replace(notAllowedCharacters, '').trim()
        if (scrubbedMetaType.length === 0) {
            return `Invalid key ${metaType}`
        }
        if (scrubbedMetaValue.length === 0) {
            return `Invalid value ${scrubbedMetaValue}`
        }
        if (scrubbedMetaType.length > MAX_KEY_LENGTH) {
            return `Key length exceeded the maximum value of ${MAX_KEY_LENGTH}. Please try a smaller key.`
        }
        if (scrubbedMetaValue.length > MAX_VALUE_LENGTH) {
            return `Value length exceeded the maximum value of ${MAX_VALUE_LENGTH}. Please try a smaller value.`
        }
        currentRace.metadata[scrubbedMetaType] = scrubbedMetaValue
        return `${scrubbedMetaType} has been set to ${scrubbedMetaValue}`
    })
    saveRace(currentRace, { metadata: currentRace.metadata })
    return statusMessages.join(', ')
}
