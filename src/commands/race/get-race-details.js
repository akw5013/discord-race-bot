const sendDirectMessage = require('../../utils/send-direct-message')
const buildRaceDetails = require('../../utils/race/build-race-details')
const getRaceByKey = require('../../db/race/get-race-by-key')
const CONSTANTS = require('../../constants')

module.exports = async (message, args) => {
    let key = null
    let shouldSendAsDM = false
    if (!args || args.length === 0) {
        key = message.channel.name || ''
        if (key.includes('-spoilers')) {
            key = key.replace('-spoilers', '')
        }
    } else {
        key = args[0]
        shouldSendAsDM = true
    }
    if (!key) { return null }
    const race = await getRaceByKey(key)
    if (!race) {
        return null
    }
    const isRaceFinished = race.status === CONSTANTS.RACE_STATUS.FINISHED
    const isOfficial = race.official
    if (isOfficial && !isRaceFinished) {
        shouldSendAsDM = true
    }
    const text = buildRaceDetails(race)
    if (shouldSendAsDM) {
        sendDirectMessage(message.author, text)
        if (message.guild) {
            message.delete()
        }
        return null
    }
    return text
}
