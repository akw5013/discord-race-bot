const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const unReadyForRace = async (message) => {
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const entrant = utils.findEntrant(currentRace, message.author)
    if (!entrant) {
        return `${message.author.username} is not currently in this race.`
    }
    if (entrant.status !== CONSTANTS.ENTRANT_STATUS.READY) {
        return `${message.author.username} cannot unready from the ${entrant.status} state.`
    }
    if (!isRaceStartable(currentRace)) {
        return `You cannot unready from a race in the ${currentRace.status} status.`
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.WAITING
    const numberUnready = currentRace.entrants.filter((ent) => ent.status !== CONSTANTS.ENTRANT_STATUS.READY).length
    saveEntrant(currentRace, entrant)
    return `${message.author.username} is no longer ready! (${numberUnready} left to ready)`
}
module.exports = unReadyForRace
