const CONSTANTS = require('../../constants')
const getEventByName = require('../../db/event/get-event-by-name')
const saveRace = require('../../db/save-race')
const { getCurrentRace } = require('./race-utils')
const saveRaceToEvent = require('../../db/event/save-race-to-event')

module.exports = async (message, args) => {
    if (!args || !args.length) {
        return 'Please specify an event name.'
    }
    const race = await getCurrentRace(message.channel.name)
    if (!race) {
        return 'There is no race happening in this channel.'
    }
    if (!race.async && race.status !== CONSTANTS.RACE_STATUS.OPEN) {
        return 'The event can only be changed for a race in the "Open" state.'
    }
    if (race.async && race.status === CONSTANTS.RACE_STATUS.FINISHED) {
        return 'The event cannot be changed for a finished async race.'
    }
    if (race.event && race.event.id) {
        return `This race is already a part of the ${race.event.name} event and it cannot be updated.`
    }
    const [eventName] = args
    const event = await getEventByName(eventName)
    if (!event) {
        return `No event with the name ${eventName} was found`
    }
    if (!event.admins.some((admin) => admin.id === message.author.id)) {
        return 'You do not have permissions to set races to this event. Only the admins of the event may do so.'
    }
    race.event = {
        id: event.id,
        name: event.name,
    }
    saveRaceToEvent(event, { id: race.id, key: race.key })
    await saveRace(race, { event: {} })
    await saveRace(race, { event: race.event })
    return 'Event successfully updated.'
}
