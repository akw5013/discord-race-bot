const getUserDetails = require('../../utils/user/get-user-details')
const utils = require('./race-utils')
const saveRace = require('../../db/save-race')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')
const updateCookies = require('../../db/user/update-cookies')

module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'You must enter an amount to donate.'
    }
    const donateAmount = Number.parseInt(args[0], 10)
    if (donateAmount <= 0 || Number.isNaN(donateAmount)) {
        return 'You must enter a positive amount to donate.'
    }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    if (currentRace.async) {
        return 'You cannot donate cookies on async races.'
    }
    if (!isRaceStartable(currentRace)) {
        return 'It is too late to donate cookies on this race.  Cookies must be donated while the race is in the Open status'
    }
    const userDetails = await getUserDetails(message)
    if (donateAmount > userDetails.cookies) {
        return `You cannot donate ${donateAmount} as you only have ${userDetails.cookies} to donate!`
    }
    const donation = {
        amount: donateAmount,
        user: userDetails.id,
        name: userDetails.name,
    }
    if (currentRace.donations) {
        currentRace.donations.push(donation)
    } else {
        currentRace.donations = [donation]
    }
    const totalDonated = currentRace.donations.reduce((sum, don) => sum + don.amount, 0)
    saveRace(currentRace, { donations: currentRace.donations })
    updateCookies(userDetails, -1 * donateAmount, '!donatecookies', `Donated cookies to ${currentRace.key}`)
    return `${message.author.username} has donated ${donateAmount} total cookies on this race!  There are now a total of ${totalDonated} added to the pool!`
}
