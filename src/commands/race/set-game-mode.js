const checkMessage = require('../../utils/check-message')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const raceUtils = require('./race-utils')
const saveRace = require('../../db/save-race')
const CONSTANTS = require('../../constants')
const securityUtils = require('../../utils/security/security-utils')

module.exports = async (message, args) => {
    if (!checkMessage(message, { checkGuild: true, checkRace: true, checkArgs: true })) {
        return `Invalid command. Usage: \`!setmode [${CONSTANTS.ALLOWED_GAME_MODES.MODES.join(', ')}]\``
    }
    const mode = args[0].toLowerCase()
    if (!CONSTANTS.ALLOWED_GAME_MODES.MODES.includes(mode)) {
        return `Invalid mode.  Please choose from [${CONSTANTS.ALLOWED_GAME_MODES.MODES.join(', ')}]`
    }
    const race = await raceUtils.getCurrentRace(message.channel.name)
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    saveRace(race, { race: { mode } })
    updateRaceMessages(message.client, race)
    return `The race mode has been updated to ${mode}.`
}
