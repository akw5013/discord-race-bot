const moment = require('moment')

const sortRaces = (race1, race2) => moment.utc(race2.created).diff(moment.utc(race1.created))
module.exports = sortRaces
