const utils = require('./race-utils')
const securityUtils = require('../../utils/security/security-utils')
const ANIMAL_LIST = require('../../../data/animals.json').animals
const COLOR_LIST = require('../../../data/colors.json').colors
const saveEntrant = require('../../db/race/save-entrant')
const isRaceStartable = require('../../utils/race/is-race-startable')

module.exports = async (message, args) => {
    if (!args || args.length < 1 || Number.isNaN(Number(args[0]))) {
        return 'The # of players per team must be sent in.'
    }
    const playersPerTeam = Math.round(args[0])
    if (playersPerTeam <= 0) {
        return 'The # of players per team must be a positive number.'
    }
    const dryRun = args.includes('--dryrun')
    const includeAll = args.includes('--everyone')
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) { return 'No race is currently active in this channel.' }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (!isRaceStartable(race)) {
        return 'Cannot set teams while the race is ongoing.'
    }
    let currentPlayers = race.entrants
    if (!includeAll) {
        currentPlayers = currentPlayers.filter((player) => !player.team || player.team === player.name)
    }
    if (currentPlayers.length < playersPerTeam) {
        return `There are not enough players in the race to make a team of ${playersPerTeam}`
    }
    if (currentPlayers.length % playersPerTeam !== 0) {
        message.channel.send('Unfortunately, there are not enough players to make an even number of teams.  Some players will be left unassigned.')
    }
    const usedColors = []
    const usedAnimals = []
    const teams = []
    const playersToPlace = currentPlayers.slice()
    while (playersToPlace.length >= playersPerTeam) {
        let color = COLOR_LIST[Math.floor(Math.random() * COLOR_LIST.length)]
        let animal = ANIMAL_LIST[Math.floor(Math.random() * ANIMAL_LIST.length)]
        const numColorTries = 0
        const numAnimalTries = 0
        const checkColor = (val) => color === val
        const checkAnimal = (val) => animal === val
        while (usedColors.some(checkColor) && numColorTries <= COLOR_LIST.length) {
            color = COLOR_LIST[Math.floor(Math.random() * COLOR_LIST.length)]
        }
        while (usedAnimals.some(checkAnimal) && numAnimalTries <= ANIMAL_LIST.length) {
            animal = ANIMAL_LIST[Math.floor(Math.random() * ANIMAL_LIST.length)]
        }
        usedColors.push(color)
        usedAnimals.push(animal)
        const newTeam = { name: `${color}-${animal}`, members: [] }
        for (let i = 0; i < playersPerTeam; i += 1) {
            const rand = Math.floor(Math.random() * (playersToPlace.length))
            const player = playersToPlace[rand]
            newTeam.members.push(player.name)
            playersToPlace.splice(rand, 1)
            if (!dryRun) {
                player.team = newTeam.name
            }
        }
        teams.push(newTeam)
    }
    if (!dryRun) {
        currentPlayers.forEach((en) => saveEntrant(race, en))
    }
    let teamsMessage = '```\n'
    teams.forEach((team) => {
        teamsMessage += `${team.name} : ${team.members.join(', ')}\n`
    })
    teamsMessage += '\n```'
    return teamsMessage
}
