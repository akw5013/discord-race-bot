const moment = require('moment')
const startRace = require('./start-race')
const CONSTANTS = require('../../constants')
const logger = require('../../utils/logger')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const getUserById = require('../../db/user/get-user-by-id')
const deleteRaceMessages = require('../../utils/race/delete-race-messages')
const updateUserDetails = require('../../utils/user/update-user-race-details')
const saveRace = require('../../db/save-race')
const getRaceByKey = require('../../db/race/get-race-by-key')
const asyncForEach = require('../../utils/async-foreach')
const finishAsyncRace = require('./finish-async-race')
const badgeProcessor = require('../../utils/badges/badge-processor')
const saveUser = require('../../db/user/save-user')
const isRaceStartable = require('../../utils/race/is-race-startable')
const processEventRace = require('../../db/event/process-event-race')
const calculateWagerAmounts = require('../../utils/race/calculate-wager-amounts')

const closeRaceInternal = async (guild, race) => {
    logger.logInfo(`Deleting all races associated with ${race.key}`)
    const textChannel = guild.channels.cache.find((channel) => channel.name === race.key)
    const voiceChannels = guild.channels.cache.filter((channel) => channel.name.includes(race.key) && channel.type === 'GUILD_VOICE')
    const spoilerChannels = guild.channels.cache.filter((channel) => race.spoilerChannels.indexOf(channel.id) >= 0)
    if (textChannel) {
        textChannel.delete()
    }
    if (voiceChannels) {
        voiceChannels.forEach((vc) => vc.delete())
    }
    if (spoilerChannels) {
        spoilerChannels.forEach((sc) => sc.delete())
    }
    deleteRaceMessages(guild.client, race)
}
const isRaceIsOver = (race) => (!race.entrants.find((ent) => ent.status === CONSTANTS.ENTRANT_STATUS.RUNNING) && race.status === CONSTANTS.RACE_STATUS.RUNNING && !race.async) // eslint-disable-line max-len
const markRaceAsCompleted = async (channel, race) => {
    const theRace = race
    if (!theRace.entrants.find((ent) => ent.status === CONSTANTS.ENTRANT_STATUS.RUNNING) && theRace.status === CONSTANTS.RACE_STATUS.RUNNING) {
    // If everyone has forfeited, just delete the race instead of logging it
        if (theRace.entrants.every((entrant) => entrant.status === CONSTANTS.ENTRANT_STATUS.FORFEIT)) {
            setImmediate(() => {
                channel.send('All participants have forfeited, the race will be cancelled.')
                channel.send(`This room and associated voice channels will be closed in ${CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MINUTES} minutes.`)
                saveRace(theRace, { status: CONSTANTS.RACE_STATUS.CANCELED })
                setTimeout(() => {
                    closeRaceInternal(channel.guild, theRace)
                }, CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MILLIS)
            })
            updateRaceMessages(channel.client, theRace)
        } else {
            const newBadges = []
            calculateWagerAmounts(theRace)
            theRace.entrants.sort((a, b) => a.placement - b.placement)
            await asyncForEach(theRace.entrants, async (entrant) => {
                const { placement, finish } = entrant
                const userInfo = await getUserById(entrant.id)
                const finisher = theRace.finishers.find((fin) => fin.id === entrant.id)
                await updateUserDetails(userInfo, {
                    placement,
                    key: theRace.key,
                    time: finish,
                    amountWon: (finisher && finisher.amountWon) || 0,
                })
                const badgesForUser = badgeProcessor(null, race, userInfo)
                if (badgesForUser && badgesForUser.length > 0) {
                    newBadges.push({ name: entrant.name, id: entrant.id, badges: badgesForUser })
                    if (!userInfo.badges) { userInfo.badges = [] }
                    userInfo.badges.push(...badgesForUser.map((badge) => ({ name: badge.name, description: badge.description, earnedFrom: badge.earnedFrom })))
                    await saveUser(userInfo, { badges: userInfo.badges })
                }
            })
            // setImmediate because we want to return the 'has finished' result first before concluding
            setImmediate(() => {
                theRace.status = CONSTANTS.RACE_STATUS.FINISHED
                theRace.finishTime = moment().toISOString(true)
                if (theRace.shouldDelete) {
                    channel.send(`All entrants have finished.  This room and associated voice channels will be closed in ${CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MINUTES} minutes.`) // eslint-disable-line max-len
                    setTimeout(() => {
                        closeRaceInternal(channel.guild, theRace)
                    }, CONSTANTS.RACE_CLOSE_TIME.AUTO_CLOSE_TIME_MILLIS)
                } else {
                    channel.send('All entrants have finished.  This room and associated voice channel will not be deleted.')
                }
                if (newBadges.length > 0) {
                    channel.send(`\`\`\`The following users have earned new badges!!!\n${newBadges.map((nb) => `${nb.name}: ${nb.badges.map((bd) => bd.name).join(', ')}`).join('\n')}\n\`\`\``) // eslint-disable-line max-len
                }
                updateRaceMessages(channel.client, theRace)
                saveRace(theRace, {
                    status: theRace.status,
                    finishTime: theRace.finishTime,
                    entrants: theRace.entrants,
                    finishers: theRace.finishers,
                }).then(() => {
                    if (theRace.event) {
                        processEventRace(theRace.event.id, theRace)
                    }
                })
            })
        }
    }
}
const findEntrant = (race, user) => race.entrants.find((entrant) => entrant.id === user.id)
const checkIfRaceIsOver = async (channel, race) => {
    if (isRaceIsOver(race)) {
        await markRaceAsCompleted(channel, race)
    }
}
const checkForRaceStart = async (message, race) => {
    if (isRaceStartable(race) && !race.entrants.find((entrant) => entrant.status !== CONSTANTS.ENTRANT_STATUS.READY)) {
        const minRaceEntrants = process.env.ENV_NAME === 'test' ? 1 : 2
        if (race.entrants.length < minRaceEntrants) {
            setImmediate(() => {
                message.channel.send(`This race cannot start without at least ${minRaceEntrants} entrants.`)
            })
        } else {
            await startRace(message.channel, race)
            await updateRaceMessages(message.client, race)
        }
    }
}
const closeRace = async (message, args, hasAdmin) => {
    const race = await getRaceByKey(message.channel.name)
    if (!race) { return }
    if (race.async && race.status === CONSTANTS.RACE_STATUS.RUNNING) {
        await finishAsyncRace(message, args, hasAdmin)
    }
    if (race.status !== CONSTANTS.RACE_STATUS.FINISHED) {
        race.status = CONSTANTS.RACE_STATUS.CANCELED
        await saveRace(race, { status: race.status })
    }
    await updateRaceMessages(message.client, race)
    await closeRaceInternal(message.guild, race)
}
module.exports = {
    getCurrentRace: getRaceByKey,
    findEntrant,
    checkIfRaceIsOver,
    closeRace,
    checkForRaceStart,
}
