const checkMessage = require('../../utils/check-message')
const buildTable = require('../../utils/table-layout')

module.exports = async (message) => {
    if (!await checkMessage(message, { channelType: 'dm' })) { return null }
    const filters = []
    filters.push({ filter: '$game type', description: 'Races that include the type specified.', example: '$game ff4fe' })
    filters.push({ filter: '$start time', description: 'Races that started after the time specified.', example: '$start 2019-01-01' })
    filters.push({ filter: '$end time', description: 'Races that finished before the time specified.', example: '$end 2019-01-01' })
    filters.push({ filter: '$numentrants num', description: 'Races that had at least num entrants.', example: '$numentrants 5' })
    filters.push({
        filter: '$ff4flags flags',
        description: 'Races that match the exact flag string.',
        example: '$ff4flags "V1 Ji Kqmt! Pk C -rescue -hobs T4gr S2 B F Nck Gl Etf Xsbk -aa -spoon -fab -huh -z"',
    })
    filters.push({ filter: '$entrant name', description: 'Races that include the specified entrant.', example: '$entrant DarkPaladin $entrant "Margaret Ann"' })
    filters.push({ filter: '$team team', description: 'Races that include the specified team name.', example: '$team Crystal-Forge' })
    filters.push({ filter: '$roller name', description: 'Races that had seeds rolled by the specified person.', example: '$roller SchalaKitty' })
    return `${'```\nUsage: !stats filters\nExample: !stats $entrant DarkPaladin $entrant Invenerable $roller SchalaKitty\n\n'}${buildTable(['Filter', 'Description', 'Example'], filters)}\n\`\`\`` // eslint-disable-line max-len
}
