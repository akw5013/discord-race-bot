const getStatHelp = require('./get-stats-help')
const getStats = require('./get-stats')
const getRaceHistory = require('./get-race-history')

module.exports = {
    '!helpstats': getStatHelp,
    '!stats': getStats,
    '!history': getRaceHistory,
}
