const moment = require('moment')
const activeRaceData = require('../race/race-data')
const sendMessage = require('../../utils/send-message')
const determineTimeByDuration = require('../../utils/determine-time-by-duration')
const applyFilters = require('../../utils/stats/apply-filters')
const parseFilters = require('../../utils/stats/parse-filters')

const getStatsFor = (racedata, filters) => {
    const numRaces = racedata.length
    let allEntrants = []
    const flatten = (ary) => ary.reduce((acc, val) => (Array.isArray(val) ? acc.concat(flatten(val)) : acc.concat(val)), [])
    allEntrants = flatten(racedata.map((race) => race.entrants))
    let allTimes = allEntrants.filter((entrant) => entrant.finish !== 'Forfeited').map((entrant) => entrant.finish)
    const forfeits = allEntrants.filter((entrant) => entrant.finish === 'Forfeited').length
    allTimes = allTimes.filter((time) => moment.duration(time).asMilliseconds() > 0)
    const avgFinish = determineTimeByDuration(moment.duration(allTimes.reduce((total, time) => total + moment.duration(time, 'HH:mm:ss.SSS').asMilliseconds(), 0) / allTimes.length)) // eslint-disable-line max-len
    const minTime = Math.min(...allTimes.map((time) => moment.duration(time).asMilliseconds()))
    const maxTime = Math.max(...allTimes.map((time) => moment.duration(time).asMilliseconds()))
    const maxFinish = determineTimeByDuration(moment.duration(maxTime))
    const minFinish = determineTimeByDuration(moment.duration(minTime))
    let stats = '```\n'
    stats += 'Applied Filters: '
    filters.forEach((filter) => {
        stats += `${filter.name} = ${filter.value} | `
    })
    stats += '\n\n'
    stats += `Number of Races: ${numRaces}\n`
    stats += `Number of Forfeits (All Entrants): ${forfeits}\n`
    stats += `Average Finish Time (All Included Races): ${avgFinish}\n`
    stats += `Fastest Finish (All Included Races): ${minFinish}\n`
    stats += `Slowest Finish (All Included Races): ${maxFinish}\n\n`
    filters.filter((f) => f.name === '$entrant' || f.name === '$team').forEach((filter) => {
        const entries = allEntrants.filter((entrant) => {
            if (filter.name === '$entrant') {
                return entrant.name.toLowerCase() === filter.value.toLowerCase()
            }
            if (filter.name === '$team') {
                return entrant.team.toLowerCase() === filter.value.toLowerCase()
            }
            return false
        })
        const forfeitsForEntrant = entries.filter((entrant) => entrant.finish === 'Forfeited').length
        const timesForEntrant = entries.map((entrant) => entrant.finish).filter((time) => moment.duration(time).asMilliseconds() > 0)
        const avgFinishByEntrant = determineTimeByDuration(moment.duration(timesForEntrant.reduce((total, time) => total + moment.duration(time, 'HH:mm:ss.SSS').asMilliseconds(), 0) / timesForEntrant.length)) // eslint-disable-line max-len
        const minTimeByEntrant = Math.min(...timesForEntrant.map((time) => moment.duration(time).asMilliseconds()))
        const maxTimeByEntrant = Math.max(...timesForEntrant.map((time) => moment.duration(time).asMilliseconds()))
        const maxFinishByEntrant = determineTimeByDuration(moment.duration(minTimeByEntrant))
        const minFinishByEntrant = determineTimeByDuration(moment.duration(maxTimeByEntrant))
        stats += `Forfeits (${filter.value}): ${forfeitsForEntrant}\n`
        stats += `Average Finish Time (${filter.value}): ${avgFinishByEntrant}\n`
        stats += `Fastest Finish (${filter.value}): ${maxFinishByEntrant}\n`
        stats += `Slowest Finish (${filter.value}): ${minFinishByEntrant}\n\n`
    })
    stats += '\n```'
    return stats
}
module.exports = async (message, args) => {
    if (message.channel.type !== 'dm') { return }
    const filters = parseFilters(args)
    const allRaceData = await activeRaceData.allRaces()
    const filteredRaceData = applyFilters(allRaceData, filters)
    if (filteredRaceData && filteredRaceData.length > 0) {
        const statsText = getStatsFor(filteredRaceData, filters)
        sendMessage(message.channel, statsText)
    } else {
        message.channel.send('No races were found matching the given criteria.')
    }
}
