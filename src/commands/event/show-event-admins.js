const getEventByName = require('../../db/event/get-event-by-name')
const buildTable = require('../../utils/table-layout')

module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return '!showeventadmins command is missing the required parameters.  `!showeventadmins event-name'
    }
    const [eventName] = args
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (!hasAdmin && existingEvent.visibility === 'private' && !existingEvent.entrants.some((en) => en.id === message.author.id)) {
        return `You do not have permission to view the details for ${eventName}`
    }
    const headers = ['Tag', 'Role']
    const adminTags = existingEvent.admins.map((admin) => ({ full: admin.full, role: admin.role }))
    return `\`\`\`\n${buildTable(headers, adminTags)}\n\`\`\``
}
