const getEventsByUser = require('../../db/event/get-events-by-user')
const tableLayout = require('../../utils/table-layout')

module.exports = async (message) => {
    const userId = message.author.id
    const events = await getEventsByUser(userId)
    if (!events) {
        return 'No events found.'
    }
    const header = ['Event Name']
    return `\`\`\`\n${tableLayout(header, events.map((e) => ({ name: e })))}\`\`\``
}
