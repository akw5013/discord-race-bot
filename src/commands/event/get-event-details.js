const getEventByName = require('../../db/event/get-event-by-name')
const buildEventDetails = require('../../utils/event/get-event-details')

module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return '!eventdetails command is missing the required parameters.  `!eventdetails event-name'
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (!hasAdmin && existingEvent.visibility === 'private' && !existingEvent.entrants.some((en) => en.id === message.author.id)) {
        return `You do not have permission to view the details for ${eventName}`
    }
    return buildEventDetails(existingEvent)
}
