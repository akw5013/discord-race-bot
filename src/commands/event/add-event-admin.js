const getEventByName = require('../../db/event/get-event-by-name')
const addEventAdmin = require('../../db/event/add-event-admin')
const findGlobalUser = require('../../utils/user/find-global-user')
const hasEventPermissions = require('../../utils/event/has-event-permissions')

const AVAILABLE_ROLES = ['Lead', 'Organizer', 'Race']

const buildHelpMessage = () => `
Usage:  !addeventadmin event-name --user "user name" --role role
    Available Roles:
        'Lead' - Is allowed to do everything the creator can do.
        'Organizer' - Can manage races and entrants.
        'Race' - Can manage races.
`

module.exports = async (message, args) => {
    if (!args || args.length === 0 || args.includes('--help')) {
        return buildHelpMessage()
    }
    if (!args.includes('--role') || !args.includes('--user')) {
        return '!addeventadmin command is missing the required parameters.  `!addeventadmin --help` for more information'
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const adminRole = args[args.indexOf('--role') + 1]
    if (!AVAILABLE_ROLES.includes(adminRole)) {
        return `Invalid role ${adminRole} specified. Please use \`!addeventadmin --help\` for more information`
    }
    const user = args[args.indexOf('--user') + 1]
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (!hasEventPermissions(existingEvent, message.author, 'Lead')) {
        return 'You do not have the necessary permissions to add other admins.'
    }
    const admin = await findGlobalUser(user, message.client)
    if (!admin) {
        return `Unable to find a user matching ${user}. Please verify they share a server with the RaceBot.`
    }
    await addEventAdmin(existingEvent, {
        id: admin.id,
        name: admin.username,
        full: admin.tag,
        role: adminRole,
    })
    return `${user} has been made an admin of ${eventName}.`
}
