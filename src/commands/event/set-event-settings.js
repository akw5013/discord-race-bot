const getEventByName = require('../../db/event/get-event-by-name')
const saveEvent = require('../../db/event/save-event')
const sendDirectMessage = require('../../utils/send-direct-message')
const createPassword = require('../../utils/event/create-password')
const encodePassword = require('../../utils/event/encode-password')
const hasEventPermissions = require('../../utils/event/has-event-permissions')

const VALID_VALUE_RULES = {
    visibility: {
        validValues: ['public', 'private'],
    },
    pointsystem: {
        validValues: ['linear', 'log', 'step', 'custom', 'glicko2'],
    },
    pointrounding: {
        validValues: ['none', 'up', 'down', 'closest'],
    },
    pointstep: {
        numeric: true,
        min: 1,
        max: 100,
    },
    pointmin: {
        numeric: true,
        min: -100,
        max: 100,
    },
    pointforfeit: {
        numeric: true,
        min: -100,
        max: 100,
    },
    pointmax: {
        numeric: true,
        min: 1,
        max: 100,
    },
    tau: {
        numeric: true,
        min: 0,
        max: 1,
    },
    rating: {
        numeric: true,
        min: 0,
        max: 3000,
    },
    rd: {
        numeric: true,
        min: 0,
        max: 2000,
    },
    vol: {
        numeric: true,
        min: 0,
        max: 1,
    },
}
/* eslint-disable max-len */
const buildHelpMessage = () => `\`\`\`
Usage:  !eventsettings event-name --options options
    Example: !eventsettings "my awesome event" --options visibility=private,pointsystem=step,pointmin=0,pointmax=20,pointstep=5,pointforfeit=0,pointrounding=none
    event-name: The name of your event. Event names are unique.
    options: A comma-separated list of key=value for the following options:
    ALL:
        key             description                                             values                         
        ---             -----------                                             ------                         
        visibility      Whether your event is public or private join            ${VALID_VALUE_RULES.visibility.validValues.join(',')}                 
    LEAGUE:
        pointsystem     The point system to use with the league.                ${VALID_VALUE_RULES.pointsystem.validValues.join(',')} 
        pointmin        Used with pointsystem to set a min number of points     Numeric value between ${VALID_VALUE_RULES.pointmin.min} and ${VALID_VALUE_RULES.pointmin.max}
        pointmax        Used with pointsystem to set a max number of points     Numeric value between ${VALID_VALUE_RULES.pointmax.min} and ${VALID_VALUE_RULES.pointmax.max}
        pointstep       Used with pointsystem to step between min and max       Numeric value between ${VALID_VALUE_RULES.pointstep.min} and ${VALID_VALUE_RULES.pointstep.max}        
        pointforfeit    Used with pointsystem to award points for a forfeit     Numeric value between ${VALID_VALUE_RULES.pointforfeit.min} and ${VALID_VALUE_RULES.pointforfeit.max}
        pointrounding   Used to determine how to round point values             ${VALID_VALUE_RULES.pointrounding.validValues.join(',')}
    glicko2 settings: (https://en.wikipedia.org/wiki/Glicko_rating_system)
        tau             Default tau for glicko2 system (default 0.5)            Numeric value between ${VALID_VALUE_RULES.tau.min} and ${VALID_VALUE_RULES.tau.max}
        rating          Default starting rating for glicko2 (default 1500)      Numeric value between ${VALID_VALUE_RULES.rating.min} and ${VALID_VALUE_RULES.rating.max}
        rd              Default rating deviation for glicko2 (default 200)      Numeric value between ${VALID_VALUE_RULES.rd.min} and ${VALID_VALUE_RULES.rd.max}
        vol             Default volatility for glicko2 (default 0.06)           Numeric value between ${VALID_VALUE_RULES.vol.min} and ${VALID_VALUE_RULES.vol.max}
\`\`\``
/* eslint-enable max-len */
const checkValidValue = (key, value) => {
    const ruleSet = VALID_VALUE_RULES[key]
    if (!ruleSet) { return false }
    if (ruleSet.numeric) {
        if (Number.isNaN(value)) { return false }
        const numValue = Number(value).toFixed(0)
        if (ruleSet.min && numValue < ruleSet.min) { return false }
        if (ruleSet.max && numValue > ruleSet.max) { return false }
    }
    if (ruleSet.validValues) {
        return ruleSet.validValues.includes(value)
    }
    return true
}

module.exports = async (message, args) => {
    if (!args || args.length === 0 || args.includes('--help')) {
        return buildHelpMessage()
    }
    if (!args.includes('--options')) {
        return '!eventsettings command is missing the required parameters.  `!eventsettings --help` for more information'
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const existingEvent = await getEventByName(eventName)
    const options = args[args.indexOf('--options') + 1].split(',')
    if (!options || options.length === 0) {
        return 'Invalid parameter values.  Use `!eventsettings --help` for more details.'
    }
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (!hasEventPermissions(existingEvent, message.author, 'Lead')) {
        return 'You do not have the necessary permissions to change event settings.'
    }
    const newSettings = { }
    let valid = true
    const invalidValues = []
    options.forEach((option) => {
        const [key, value] = option.split('=')
        if (!checkValidValue(key, value)) {
            valid = false
            invalidValues.push({ key, value })
        }
        newSettings[key] = value
    })
    if (!valid) {
        return `Invalid parameter values: ${invalidValues.map((o) => `${o.key}:${o.value}`).join(' ')}.  Use \`!eventsettings --help\` for more details.`
    }
    if (newSettings.visibility && newSettings.visibility === 'private') {
        const joinPassword = createPassword()
        newSettings.joinPassword = encodePassword(joinPassword)
        sendDirectMessage(message.author, `The password to join your event is ${joinPassword}`)
    }
    await saveEvent(existingEvent, newSettings)
    return `${eventName} settings modified successfully.`
}
