const SIXTEEN_BIT_MAX = 18446744073709551615n
module.exports = async () => {
    const lowest = 1n
    const difference = SIXTEEN_BIT_MAX - lowest
    const differenceLength = difference.toString().length
    let multiplier = ''
    while (multiplier.length < differenceLength) {
        multiplier += Math.random()
            .toString()
            .split('.')[1]
    }
    multiplier = multiplier.slice(0, differenceLength)
    const divisor = `1${'0'.repeat(differenceLength)}`
    const randomDifference = (difference * BigInt(multiplier)) / BigInt(divisor)
    return (lowest + randomDifference).toString()
}
