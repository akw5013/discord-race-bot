const fetch = require('node-fetch')
const flagBuilder = require('../utils/flagstring-builder')
const getRandomFlags = require('../utils/get-random-flags')
const buildUserFlags = require('../utils/build-user-flags')

const flagApiUrl = 'https://randomizer.smrpgspeedruns.com/api/v1/flags'

const parseFlagObject = (flagObj, mode, flagGroup) => {
    const {
        value,
        modes,
        choices,
        options,
    } = flagObj
    if (!modes.includes(mode)) {
        return null
    }
    const flagToSet = flagGroup || value
    let flagValue = null
    if (options.length === 0 && choices.length === 0) {
        flagValue = value
        if (typeof flagValue === 'object') {
            return parseFlagObject(flagValue, mode, flagToSet)
        }
    } else {
        const choiceFlags = choices.map((choice) => parseFlagObject(choice, mode, value))
        const optionFlags = options.map((opt) => ({
            group: value,
            flag: opt.value,
        }))
        flagValue = { }
        if (choiceFlags.length > 0) {
            flagValue.unique = []
            flagValue.unique.push(...choiceFlags)
        }
        if (optionFlags.length > 0) {
            flagValue.optional = []
            flagValue.optional.push(...optionFlags)
        }
    }
    return { group: flagToSet, flag: flagValue }
}

module.exports = async (message, args) => {
    const flagOptions = await fetch(flagApiUrl).then((res) => res.json())
    const mode = 'open'
    const flagGroups = { }
    flagOptions.flags.forEach((fo) => {
        flagGroups[fo.value] = { unique: [parseFlagObject(fo, mode)] }
    })
    const userFlags = buildUserFlags(args)
    const selectedFlags = getRandomFlags(flagGroups, userFlags)
    const forcedFlags = {
        '-palette': true,
    }
    return flagBuilder(selectedFlags, forcedFlags).replace('/', '')
}
