const generateFlags = require('./smrpgflags')
const randomFlags = require('./random-flags')

module.exports = {
    '!smrpgflags': generateFlags,
    '!smrpgrandom': randomFlags,
}
