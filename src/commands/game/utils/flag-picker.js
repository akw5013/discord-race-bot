const getRandomNumber = require('../../../utils/get-random-number')

const getFlags = (option) => {
    if (typeof option === 'object') {
        const keys = Object.keys(option)
        const value = getRandomNumber(0, keys.length - 1)
        const selectedKey = keys[value]
        const keyOptions = option[selectedKey]
        const selectedOption = keyOptions[getRandomNumber(0, keyOptions.length - 1)]
        return selectedKey + getFlags(selectedOption)
    }
    return option
}

module.exports = (availableFlags, startingFlags, flagOptions) => {
    const hasSetFlags = (flagOptions !== null && flagOptions !== undefined)
    const currentFlags = startingFlags || { }
    let flags = ''

    Object.keys(availableFlags).forEach((flag) => {
        const shouldUseFlag = (getRandomNumber(0, 100) > availableFlags[flag].weight)
        if (currentFlags[flag]) { return }
        if (!shouldUseFlag) { return }
        currentFlags[flag] = []
        const uniqueOptions = availableFlags[flag].unique
        const optionalOptions = availableFlags[flag].optional
        if (uniqueOptions) {
            const selectedValue = uniqueOptions[getRandomNumber(0, uniqueOptions.length - 1)]
            currentFlags[flag].push(getFlags(selectedValue))
        }
        if (optionalOptions) {
            const minOptions = availableFlags[flag].required || 0
            const numOptions = getRandomNumber(0, optionalOptions.length) + minOptions
            if (numOptions === optionalOptions.length) {
                optionalOptions.forEach((option) => currentFlags[flag].push(getFlags(option)))
            } else {
                for (let i = 0; i < numOptions;) {
                    const randomOption = optionalOptions[getRandomNumber(0, optionalOptions.length - 1)]
                    if (currentFlags[flag].indexOf(randomOption) < 0) {
                        currentFlags[flag].push(getFlags(randomOption))
                        i += 1
                    }
                }
            }
        }
    })

    if (hasSetFlags) {
        flagOptions.filter((arg) => !arg.startsWith('-')).forEach((arg) => {
            const [flag] = arg
            if (availableFlags[flag]) {
                if (currentFlags[flag]) {
                    currentFlags[flag] = Object.assign(currentFlags[flag], [...arg].slice(1))
                } else {
                    currentFlags[flag] = [...arg].slice(1)
                }
            }
        })
        flagOptions.filter((arg) => arg.startsWith('-')).forEach((arg) => {
            const [flag] = arg
            currentFlags[flag] = true
        })
    }

    Object.keys(currentFlags).forEach((flag) => {
        const flagValue = currentFlags[flag]
        if (flag.length === 1) {
            flags += ` ${flag}`
            if (flagValue && flagValue.length > 0) {
                flags += `${currentFlags[flag].join('')}`
            }
            return
        }
        if (flagValue === '' || flagValue === ' ' || flagValue.length === 0) { return }
        if (flagValue === true) {
            flags += ` ${flag}`
        } else if (flagValue) {
            const deduped = new Set(flagValue)
            flags += ` ${Array.from(deduped).join(' ')}`
        }
    })
    return flags
}
