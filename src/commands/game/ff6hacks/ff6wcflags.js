const randomstring = require('randomstring')
const fetch = require('node-fetch')
const utils = require('../../race/race-utils')
const securityUtils = require('../../../utils/security/security-utils')
const saveRace = require('../../../db/save-race')
const isRaceEditable = require('../../../utils/race/is-race-editable')

const baseUrl = 'https://api.ff6worldscollide.com/api/seed'
const SEED_LENGTH = 16

const setFlags = (flagString, description) => {
    const seed = randomstring.generate({ length: SEED_LENGTH, charset: 'alphanumeric', capitalization: 'uppercase' })
    const safeDescription = Array.isArray(description) ? description.join(' ') : description
    const safeFlags = Array.isArray(flagString) ? flagString.join(' ') : flagString
    return {
        seed,
        flags: safeFlags,
        description: safeDescription,
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace)) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const metadata = {
        Seed: seedDetails.seed,
        Flags: seedDetails.flags,
        Url: seedDetails.url,
        Roller: message.author.username,
        Description: seedDetails.description,
    }
    if (currentRace.metadata) {
        currentRace.metadata = Object.assign(currentRace.metadata, metadata)
    } else {
        currentRace.metadata = metadata
    }
    message.channel.setTopic(`${seedDetails.url}`)
    saveRace(currentRace, { metadata: currentRace.metadata })
}
module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    const hasDescription = args.includes('--description')
    let flags
    let desc
    if (hasDescription) {
        const descIndex = args.indexOf('--description')
        flags = args.slice(0, descIndex)
        desc = args.slice(descIndex + 1)
    } else {
        flags = args.join(' ')
        desc = ''
    }
    const flagInfo = setFlags(flags, desc)
    const postBody = {
        key: process.env.FF6WORLDSCOLLIDE_API_KEY,
        flags: `${flagInfo.flags} -s ${flagInfo.seed}`,
    }
    if (flagInfo.description) {
        postBody.description = flagInfo.description
    }
    const response = await fetch(baseUrl, {
        method: 'POST',
        body: JSON.stringify(postBody),
    })
    const json = await response.json()
    const { errors = [], url } = json
    if (errors.length) {
        return `Error creating seed: ${errors.join('. ')}`
    }
    await updateRaceDetails(message, {
        seed: flagInfo.seed,
        flags: flagInfo.flags,
        url,
        description: desc,
    }, hasAdmin)
    return `Your seed can be accessed at ${url}`
}
