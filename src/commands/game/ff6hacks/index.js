const generateLegacy = require('./ff6wclegacy')
const generateFlags = require('./ff6wcflags')

module.exports = {
    '!ff6wcflags': generateFlags,
    '!ff6wclegacy': generateLegacy,
}
