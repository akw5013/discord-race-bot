const ff4hacks = require('./ff4hacks')
const ff1hacks = require('./ff1hacks')
const ff6hacks = require('./ff6hacks')
const smrpghacks = require('./smrpghacks')
const smb3r = require('./smb3r')
const hades = require('./hades')
const stswf = require('./stswf')
const ctjot = require('./ctjot')
const ff4fd = require('./ff4fd')
const CONSTANTS = require('../../constants')

const displayGames = () => {
    const gamesList = CONSTANTS.ALLOWED_GAMES.GAMES.join(', ')
    const additionallySupportedGames = 'ff1r, ff4fe, smrpgr, ff6wc, smb3r, hades, stswf, ctjot'
    return `The currently allowed games are ${gamesList}.\nThe following games have additional custom support: ${additionallySupportedGames}`
}

const gameCommands = Object.assign({}, ...[ff4hacks, ff1hacks, smrpghacks, ff6hacks, smb3r, hades, stswf, ctjot, ff4fd])
gameCommands['!games'] = displayGames
module.exports = gameCommands
