const baseUrl = 'https://ff4fe.galeswift.com/api'
const fetch = require('node-fetch')
const randomstring = require('randomstring')
const sendMessage = require('../../../utils/send-message')
const sendDirectMessage = require('../../../utils/send-direct-message')
const utils = require('../../race/race-utils')
const logger = require('../../../utils/logger')
const saveRace = require('../../../db/save-race')
const securityUtils = require('../../../utils/security/security-utils')
const isRaceEditable = require('../../../utils/race/is-race-editable')
const asyncForEach = require('../../../utils/async-foreach')
const findGlobalUser = require('../../../utils/user/find-global-user')

const MAX_TRIES = 3
const ff4apikey = process.env.FE_API_KEY_GALESWIFT
const SEED_LENGTH = 10
const setFlags = (flagString) => {
    const seed = randomstring.generate(SEED_LENGTH).toUpperCase()
    return {
        seed,
        flags: flagString,
    }
}
const sendSeedMessage = async (message, seedDetails, shouldDm) => {
    const { channel, client } = message
    const currentRace = await utils.getCurrentRace(channel.name)
    if (currentRace && shouldDm) {
        await asyncForEach(currentRace.entrants, async (entrant) => {
            const user = await findGlobalUser(entrant.name, client)
            await sendDirectMessage(user, `Here is your seed for ${currentRace.key}: ${seedDetails.url}`)
        })
        if (!currentRace.entrants.some((e) => e.id === message.author.id)) {
            await sendDirectMessage(message.author, `Here is the seed you rolled for ${currentRace.key}: ${seedDetails.url} . This is your problem now.`)
        }
        sendMessage(channel, `All ${currentRace.entrants.length} entrants and the roller should have received a DM with the seed details.`)
    } else {
        sendMessage(channel, `The seed has been generated. Please use ${seedDetails.url} to play the seed.`)
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin, hiddenFlags = false, shouldDm = false) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace) && !hasAdmin) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const metadata = {
        Version: seedDetails.version,
        Seed: shouldDm ? '<hidden>' : seedDetails.seed,
        Flags: hiddenFlags ? '<hidden>' : seedDetails.flags,
        Url: shouldDm ? '<hidden>' : seedDetails.url,
        Hash: seedDetails.verification,
        Roller: message.author.username,
    }
    if (currentRace.metadata) {
        currentRace.metadata = Object.assign(currentRace.metadata, metadata)
    } else {
        currentRace.metadata = metadata
    }
    message.channel.setTopic(`${shouldDm ? 'Seed DMd to Racers' : seedDetails.url} : ${seedDetails.verification}`)
    saveRace(currentRace, { metadata: currentRace.metadata })
}
module.exports = async (message, args, hasAdmin, url = baseUrl) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    let hidden = false
    let sendViaDm = false
    if (args.includes('--hidden')) {
        hidden = true
    }
    if (args.includes('--dm')) {
        sendViaDm = true
    }
    const fullFlagset = args.join(' ').replace('--hidden', '').replace('--dm', '').trim()
    const flagInfo = setFlags(fullFlagset)
    let taskId = null
    let seedId = null
    const generationUrl = `${url}/generate?key=${ff4apikey}`
    const postBody = {
        flags: flagInfo.flags,
        seed: flagInfo.seed,
    }
    if (hidden) {
        postBody.metaconfig = { hide_flags: true }
        if (message.guild) {
            message.delete()
        }
    }
    let tries = 0
    fetch(generationUrl, {
        method: 'post',
        body: JSON.stringify(postBody),
    })
        .then((res) => res.json())
        .then((json) => {
            if (json.status === 'ok') {
                taskId = json.task_id
            } else if (json.status === 'exists') {
                seedId = json.seed_id
            } else {
                return json
            }
            return null
        })
        .then((error) => {
            if (error) {
                sendMessage(message.channel, `Error generating seed: ${error.error}`)
            }
            if (taskId) {
                let processing = false
                const poll = setInterval(() => {
                    const detailsUrl = `${url}/task?key=${ff4apikey}&id=${taskId}`
                    if (!processing) {
                        if (tries > MAX_TRIES) {
                            clearInterval(poll)
                            sendMessage(message.channel, 'Maximum attempts reached. No longer polling for seed details.')
                        }
                        processing = true
                        fetch(detailsUrl)
                            .then((res) => res.json())
                            .then((json) => {
                                if (json.seed_id) {
                                    seedId = json.seed_id
                                    clearInterval(poll)
                                    const seedUrl = `${url}/seed?key=${ff4apikey}&id=${seedId}`
                                    fetch(seedUrl)
                                        .then((res) => res.json())
                                        .then(async (seedDetails) => {
                                            await updateRaceDetails(message, seedDetails, hasAdmin, hidden, sendViaDm)
                                            await sendSeedMessage(message, seedDetails, sendViaDm)
                                        })
                                        .catch((err) => {
                                            tries += 1
                                            sendMessage(message.channel, `Error reading API response: ${err}`)
                                        })
                                } else if (json.error) {
                                    logger.logError(`Error: ${json}`)
                                    tries += 1
                                    sendMessage(message.channel, `Error generating seed: ${json.error}`)
                                    clearInterval(poll)
                                }
                                processing = false
                            })
                            .catch((err) => {
                                processing = false
                                sendMessage(message.channel, `Error talking to FE API: ${err}`)
                            })
                    }
                }, 1000)
            } else if (seedId) {
                const seedUrl = `${url}/seed?key=${ff4apikey}&id=${seedId}`
                fetch(seedUrl)
                    .then((res) => res.json())
                    .then(async (seedDetails) => {
                        await updateRaceDetails(message, seedDetails, hasAdmin, hidden, sendViaDm)
                        await sendSeedMessage(message, seedDetails, sendViaDm)
                    })
            }
        })
    return 'Seed generation pending...'
}
