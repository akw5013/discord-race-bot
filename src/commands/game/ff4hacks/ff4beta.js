const baseUrl = 'http://beta.ff4fe.com/api'
const ff4flags = require('./ff4flags')

module.exports = async (message, args, hasAdmin) => ff4flags(message, args, hasAdmin, baseUrl)
