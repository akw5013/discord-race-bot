const ff4flags = require('./ff4flags')
const ff4beta = require('./ff4beta')
const omg = require('./omg')
const zz6 = require('./zz6')
const ff4galeswift = require('./ff4galeswift')

module.exports = {
    '!ff4flags': ff4flags,
    '!ff4beta': ff4beta,
    '!omg': omg,
    '!zz6': zz6,
    '!ff4galeswift': ff4galeswift
}
