const randomString = require('randomstring')
const sendMessage = require('../../../utils/send-message')

module.exports = (message) => {
    const randomSeed = randomString.generate({
        length: 12,
        charset: 'numeric',
    })
    sendMessage(message.channel, randomSeed)
}
