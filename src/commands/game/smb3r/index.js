const generateSeed = require('./generate-seed')

module.exports = {
    '!smb3rseed': generateSeed,
}
