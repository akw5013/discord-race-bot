const chooseAscension = require('./choose-ascension')
const chooseClass = require('./choose-class')

module.exports = {
    '!ascension': chooseAscension,
    '!class': chooseClass,
    '!classes': chooseClass,
}
