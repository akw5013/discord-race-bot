const CLASSES = [
    'Ironclad (Red)',
    'Silent (Green)',
    'Defect (Blue)',
    'Watcher (Purple)',
]
const getRandomOption = require('../../../utils/get-random-option')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')

const maxClasses = 5
module.exports = async (message, args) => {
    let numberOfClasses = 1
    if (args && args.length) {
        numberOfClasses = Number(args[0])
        if (Number.isNaN(numberOfClasses) || !numberOfClasses || !Number.isFinite(numberOfClasses)) {
            numberOfClasses = 1
        }
    }
    numberOfClasses = Math.min(numberOfClasses, maxClasses)
    const classesRolled = []
    for (let rolledClasses = 0; rolledClasses < numberOfClasses; rolledClasses += 1) {
        const randomClass = getRandomOption(CLASSES)
        classesRolled.push(randomClass)
    }
    const activeRace = await utils.getCurrentRace(message.channel.name)
    if (activeRace) {
        const classMetadata = { }
        classesRolled.forEach((s, indx) => {
            classMetadata[`Class${indx + 1}`] = s
        })
        activeRace.metadata = Object.assign(activeRace.metadata || {}, classMetadata)
        await saveRace(activeRace, { metadata: activeRace.metadata })
    }
    return `${classesRolled.join(', ')}`
}
