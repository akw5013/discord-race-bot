const randomstring = require('randomstring')
const utils = require('../../race/race-utils')
const securityUtils = require('../../../utils/security/security-utils')
const saveRace = require('../../../db/save-race')
const isRaceEditable = require('../../../utils/race/is-race-editable')

const baseUrl = 'https://4-6-4.finalfantasyrandomizer.com'
const SEED_LENGTH = 8

const setFlags = (flagString) => {
    const seed = randomstring.generate({ length: SEED_LENGTH, charset: 'hex', capitalization: 'uppercase' })
    return {
        seed,
        flags: flagString,
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace)) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const metadata = {
        Seed: seedDetails.seed,
        Flags: seedDetails.flags,
        Url: seedDetails.url,
        Roller: message.author.username,
    }
    if (currentRace.metadata) {
        currentRace.metadata = Object.assign(currentRace.metadata, metadata)
    } else {
        currentRace.metadata = metadata
    }
    message.channel.setTopic(`${seedDetails.url}`)
    saveRace(currentRace, { metadata: currentRace.metadata })
}
module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    const fullFlagset = args.join(' ')
    const flagInfo = setFlags(fullFlagset)
    const generationUrl = `${baseUrl}/?s=${flagInfo.seed}&f=${flagInfo.flags}`
    await updateRaceDetails(message, { seed: flagInfo.seed, flags: flagInfo.flags, url: generationUrl }, hasAdmin)
    return `Your seed can be accessed at ${generationUrl}`
}
