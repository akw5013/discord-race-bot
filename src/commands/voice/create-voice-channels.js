const Discord = require('discord.js')
const utils = require('../race/race-utils')
const logger = require('../../utils/logger')
const CONSTANTS = require('../../constants/index')
const getConfigValue = require('../../utils/get-config-value')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const getChannelName = (race, team) => `${race.key}-${team.replace(/ /, '-')}`

module.exports = async (message, args, isAdmin) => {
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (!isRaceStartable(race)) {
        return `Cannot create voice channels for a race in the ${race.status} status.`
    }
    const teams = new Set(race.entrants.map((entrant) => entrant.team))
    const raceCategory = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.CATEGORY_KEY, CONSTANTS.CONFIG.DEFAULT_RACE_CATEGORY)
    const maxAllowedChannels = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAX_VOICE_ADMIN, CONSTANTS.CONFIG.DEFAULT_MAX_VOICE)
    if (teams.size >= maxAllowedChannels) {
        if (!isAdmin) {
            return 'The maximum number of voice channels that can be created by a non-admin has been reached.  Please ask an admin to create the voice channels.'
        }
    }
    const adminRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.BOT_ADMIN_ROLE, CONSTANTS.CONFIG.DEFAULT_ADMIN_ROLE)
    const botRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_ROLE, message.client.user.username)
    const activeVoiceChannels = message.guild.channels.cache.filter((channel) => channel.type === 'GUILD_VOICE' && channel.parent && channel.parent.name === raceCategory)
    logger.logDebug(`There are ${activeVoiceChannels.size} active voice channels`)
    if (activeVoiceChannels.size >= maxAllowedChannels) {
        if (!isAdmin) {
            return 'The maximum number of voice channels that can be created by a non-admin has been reached.  Please ask an admin to create the voice channels.'
        }
    }
    const channelList = [...teams].map((team) => getChannelName(race, team))
    let parent
    if (raceCategory) {
        const cat = message.guild.channels.cache.find((ch) => ch.name === raceCategory)
        if (cat) {
            parent = cat
        }
    }
    const adminGuildRole = message.guild.roles.cache.find((role) => role.name === adminRole)
    const atEveryone = message.guild.roles.cache.find((role) => role.name === '@everyone')
    let guildBotRole = message.guild.roles.cache.find((role) => role.name === botRole)
    // Backwards compatibility jank
    if (!guildBotRole) {
        guildBotRole = message.guild.roles.cache.find((role) => role.name === botRole.replace('discord', 'dr'))
        if (!guildBotRole) {
            guildBotRole = message.guild.roles.cache.find((role) => role.name === botRole.replace('dr', 'discord'))
        }
    }
    const viewChannel = new Discord.Permissions(Discord.Permissions.FLAGS.VIEW_CHANNEL)
    const teamPermissions = new Discord.Permissions(Discord.Permissions.FLAGS.CONNECT); // semi not optional
    [
        Discord.Permissions.FLAGS.SPEAK,
        Discord.Permissions.FLAGS.CONNECT,
        Discord.Permissions.FLAGS.USE_VAD,
        Discord.Permissions.FLAGS.VIEW_CHANNEL,
    ].forEach((flag) => teamPermissions.add(flag))
    const adminPermissions = new Discord.Permissions(Discord.Permissions.FLAGS.MOVE_MEMBERS); // semi not optional
    [
        Discord.Permissions.FLAGS.SPEAK,
        Discord.Permissions.FLAGS.CONNECT,
        Discord.Permissions.FLAGS.USE_VAD,
        Discord.Permissions.FLAGS.MANAGE_CHANNELS,
        Discord.Permissions.FLAGS.VIEW_CHANNEL,
    ].forEach((flag) => adminPermissions.add(flag))
    teams.forEach((team) => {
        const channelName = getChannelName(race, team)
        const existingChannel = message.guild.channels.cache.find((channel) => channel.name === channelName && channel.type === 'GUILD_VOICE')
        const teamMembers = race.entrants.filter((entrant) => entrant.team === team)
        if (!existingChannel) {
            message.guild.channels.create(channelName, {
                type: 'GUILD_VOICE',
                parent,
                permissionOverwrites: [
                    {
                        id: adminGuildRole.id,
                        allow: adminPermissions,
                    },
                    {
                        id: guildBotRole.id,
                        allow: adminPermissions,
                    },
                    {
                        id: atEveryone.id,
                        deny: viewChannel,
                    },
                    ...teamMembers.map((teamMember) => ({
                        id: teamMember.id,
                        allow: teamPermissions,
                    })),
                ],
            })
        } else {
            teamMembers.forEach((teamMember) => {
                existingChannel.permissionOverwrites.create(teamMember.id, {
                    VIEW_CHANNEL: true, SPEAK: true, CONNECT: true, USE_VAD: true,
                })
            })
        }
    })
    // Delete all unused voice channels
    // eslint-disable-next-line max-len
    const unusedVoiceChannels = message.guild.channels.cache.filter((channel) => channel.name.includes(race.key) && channel.type === 'GUILD_VOICE' && channelList.indexOf(channel.name) < 0)
    if (unusedVoiceChannels && unusedVoiceChannels.size > 0) {
        unusedVoiceChannels.forEach((ch) => ch.delete())
    }
    return null
}
