const currentCache = require('./master-cache').guild
const cacheUtils = require('./cache-utils')

const getValue = (id) => cacheUtils.getValue(currentCache, id)
const updateCacheValue = (id, value) => cacheUtils.updateCacheValue(currentCache, id, value)
const clearCacheValue = (id) => cacheUtils.clearCacheValue(currentCache, id)

module.exports = {
    getValue,
    updateCacheValue,
    clearCacheValue,
}
