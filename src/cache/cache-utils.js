const { Mutex, withTimeout } = require('async-mutex')

const mutexWithTimeout = withTimeout(new Mutex(), 100)

const getValue = async (cache, id) => {
    if (cache && cache.enabled) {
        return mutexWithTimeout.runExclusive(() => cache[id])
    }
    return null
}

const updateCacheValue = async (cache, id, value) => {
    const updatedCache = cache
    if (id && value && updatedCache && updatedCache.enabled) {
        await mutexWithTimeout.runExclusive(() => {
            const newId = id.toLowerCase()
            updatedCache[newId] = value
        })
    }
}
const clearCacheValue = async (cache, id) => {
    const updatedCache = cache
    if (id) {
        const newId = id.toLowerCase()
        await mutexWithTimeout.runExclusive(() => {
            if (updatedCache && updatedCache.enabled && updatedCache[newId]) {
                delete updatedCache[newId]
            }
        })
    }
}
module.exports = {
    getValue,
    updateCacheValue,
    clearCacheValue,
}
