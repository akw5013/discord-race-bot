const entrantStatus = require('./entrant-status')
const raceStatus = require('./race-status')
const config = require('../../config.json')
const serverConfig = require('./server-config-key')
const allowedGames = require('./allowed-games')
const allowedGameModes = require('./allowed-game-modes')
const discordChannelPermissions = require('./discord-channel-permissions')
const raceCloseTime = require('./race-close-time')
const currentFeatures = require('./current-features')
const eventPermissions = require('./event-permissions')

module.exports = {
    ENTRANT_STATUS: entrantStatus,
    RACE_STATUS: raceStatus,
    CONFIG: config,
    SERVER_CONFIG: serverConfig,
    ALLOWED_GAMES: allowedGames,
    ALLOWED_GAME_MODES: allowedGameModes,
    PERMISSION_SETS: discordChannelPermissions,
    RACE_CLOSE_TIME: raceCloseTime,
    ACTIVE_FEATURES: currentFeatures,
    EVENT_PERMISSIONS: eventPermissions,
}
