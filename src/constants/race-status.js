module.exports = {
    OPEN: 'Open',
    LOCKED: 'Locked',
    RUNNING: 'Running',
    FINISHED: 'Completed',
    CANCELED: 'Cancelled',
    SCHEDULED: 'Scheduled',
    STARTING: 'Starting',
}
